import React from 'react';
import { PropTypes } from 'prop-types';
import './Arrows.css';

/* ReflexiveArrow Functional Component
 * ==========================
 * Renders a reflexive curved Arrow based on the required props (x1,y1 and x2,y2 are the endpoints).
 * Optional props (color, strokeWidth) determine the styling, otherwise they default to (black, 2)
 *
 * IMPLEMENTATION DETAILS:
 * The arrow path will be the large arc of an ellipse connecting the two points. 
 * A marker will be placed on the end of the path that orients along the path. Because the slope can change quickly, we set refX=0 in order to make sure the orientation of the arrowhead aligns with the final slope of the path.
 *
 * LOGIC:
 * If the arrow goes into quadrants II or IV (in mathworld) from the origin, the signs of dX and dY (in SVG world) are the same, which means that we should draw the arrow Clockwise, or have a positively-angled sweep (in SVG world), so we should set the sweep-flag to 1. 
 */
const ReflexiveArrow = (props) => {
	let dX = (props.x2 - props.x1)/2;
	let dY = (props.y2 - props.y1)/2;

	return(
		<g id={props.id} className='arrow'>
			<defs>
				<marker
					id={props.id+"-head"}
					orient='auto'
					markerWidth='2' markerHeight='3'
					refX='0' refY='1.5'
				>
					<path d='M0,0 V3 L2,1.5 Z' fill={props.color} />
				</marker>
			</defs>
			<path
				markerEnd={`url(#${props.id+"-head"})`}
				d={`
					M ${props.x1}, ${props.y1}
					A ${1.5*dX} ${1.5*dY} ${0} ${1} ${Math.sign(dX) === Math.sign(dY) ? 1 : 0} ${props.x2} ${props.y2}
				`}
				fill='none'
				style={{stroke: props.color, strokeWidth: props.strokeWidth}}
			/>
		</g>
	);
};

ReflexiveArrow.propTypes = {
	id: PropTypes.string.isRequired,
	x1: PropTypes.number.isRequired,
	y1: PropTypes.number.isRequired,
	x2: PropTypes.number.isRequired,
	y2: PropTypes.number.isRequired,
	color: PropTypes.string,
	strokeWidth: PropTypes.number
};
ReflexiveArrow.defaultProps = {
	color: 'black',
	strokeWidth: 2
};

export default ReflexiveArrow;