import React from 'react';
import { PropTypes } from 'prop-types';
import './Arrows.css';

/* Arrow Functional Component
 * ==========================
 * Renders a curved sigmoid Arrow based on the required props (x1,y1 and x2,y2 are the endpoints).
 * Optional props (color, strokeWidth) determine the styling, otherwise they default to (black, 2)
 *
 * IMPLEMENTATION DETAILS:
 * The arrow path will be made of two 's' (symmetric relative) segments, which creates a symmetric control point around the starting point of the segment, or places it on the prior point if one isn't available to copy (so there is no control handle). This means we get two symmetrically-placed control points around the second point of the path (between the start and end).
 * A marker will be placed on the end of the path that orients along the path. Because the slope can change quickly, we set refX=0 in order to make sure the orientation of the arrowhead aligns with the final slope of the path.
 *
 * LOGIC:
 * First, we have to figure out where to place the midpoint of the path - it will be halfway between the start and end points at the relative position of dX,dY from x1,y1.
 * Next, we calculate the first control point for that midpoint of the enclosing rectangle, which will be on the long edge whose endpoint is the starting point, at a distance of half |the major axis| minus half |the minor axis|,
 * e.g.: |dX| - |dY| if x is the major axis, or |dY| - |dX| if y is.
 */
const Arrow = (props) => {
console.log("Arrow rendered");
	let dX = (props.x2 - props.x1)/2;
	let dY = (props.y2 - props.y1)/2;
	/* The formulation of the midPathControlPoint depends on two factors: whether the signs of dX and dY are the same, and then, which is the major axis. We take advantage of the signed nature of dX and dY to simply decide whether to add or subtract them*/
	let midPathControlPoint = "";
	if (Math.sign(dX) === Math.sign(dY)){//signs are the same
		midPathControlPoint = Math.abs(dX) > Math.abs(dY)
			? `${dX-dY},0` //x is the major axis
			: `0,${dY-dX}`; //y is the major axis
	} else {//signs are opposite
		midPathControlPoint = Math.abs(dX) > Math.abs(dY)
			? `${dX+dY},0` //x is the major axis
			: `0,${dY+dX}`; //y is the major axis
	}

	return(
		<g id={props.id} className='arrow'>
			<defs>
				<marker
					id={props.id+"-head"}
					orient='auto'
					markerWidth='2' markerHeight='3'
					refX='0' refY='1.5'
				>
					<path d='M0,0 V3 L2,1.5 Z' fill={props.color} />
				</marker>
			</defs>
			<path
				markerEnd={`url(#${props.id+"-head"})`}
				/* The mid-point of the path, the first endpoint of the first s segment, {dX} {dY}, depends on which the minor axis of the rectangle is, which we can determine by testing whether dY > dX. (if so, Y is the long axis). The concept of the path is: three path points (start, midpoint, end), off by (dx, dy) from the prior one. The control points are located on the long sides of the rectangle, separated from the midpoint by (dMinorAxis, dMinorAxis) at +135 and -45 degrees from the long axis.*/
				d={`
					M ${props.x1}, ${props.y1}
					s ${midPathControlPoint} ${dX}, ${dY}
					s ${dX}, ${dY} ${dX}, ${dY}
				`}
				fill='none'
				style={{stroke: props.color, strokeWidth: props.strokeWidth}}
			/>
		</g>
	);
};

Arrow.propTypes = {
	id: PropTypes.string.isRequired,
	x1: PropTypes.number.isRequired,
	y1: PropTypes.number.isRequired,
	x2: PropTypes.number.isRequired,
	y2: PropTypes.number.isRequired,
	color: PropTypes.string,
	strokeWidth: PropTypes.number
};
Arrow.defaultProps = {
	color: 'black',
	strokeWidth: 2
};

export default Arrow;