import React from 'react';
import { PropTypes } from 'prop-types';
import ReflexiveArrowFromPoints from './ReflexiveArrowFromPoints.js';

/* ReflexiveArrowFromLine Functional Component
 * ==================================
 * Composes a ReflexiveArrowFrom Points Component, allowing you to pass a single "line" object one that has 'start' and 'end' properties, the value of each of which is a {x, y} point.
 */
const ReflexiveArrowFromLine = (props) => {
	return (
		<ReflexiveArrowFromPoints
			id={props.id}
			start={props.line.start}
			end={props.line.end}
			color={props.color}
			strokeWidth={props.strokeWidth}
		/>
	);
};

ReflexiveArrowFromLine.propTypes = {
	id: PropTypes.string.isRequired,
	line: PropTypes.object.isRequired,
	color: PropTypes.string,
	strokeWidth: PropTypes.number
};

export default ReflexiveArrowFromLine;