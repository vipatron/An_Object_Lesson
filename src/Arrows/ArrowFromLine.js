import React from 'react';
import { PropTypes } from 'prop-types';
import ArrowFromPoints from './ArrowFromPoints.js';

/* ArrowFromLine Functional Component
 * ==================================
 * Composes a ArrowFrom Points Component, allowing you to pass a single "line" object one that has 'start' and 'end' properties, the value of each of which is a {x, y} point.
 * 
 * Implementation: No performance boost from using PureComponent because even if the start.x/y & end.x/y are the same, it is likely that a new line OBJECT being passed as props, so it's better to keep the code terse.
 */
const ArrowFromLine = props => 
	<ArrowFromPoints
		id={props.id}
		start={props.line.start}
		end={props.line.end}
		color={props.color}
		strokeWidth={props.strokeWidth}
	/>;

ArrowFromLine.propTypes = {
	id: PropTypes.string.isRequired,
	line: PropTypes.object.isRequired,
	color: PropTypes.string,
	strokeWidth: PropTypes.number
};

export default ArrowFromLine;