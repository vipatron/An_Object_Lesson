import React from 'react';
import { PropTypes } from 'prop-types';
import Arrow from './Arrow.js'

/* ArrowFromPoints Functional Component
 * ====================================
 * Composes an Arrow Component, allowing you to assign {x, y} Point Objects as props for 'start' and 'end', instead of specifying x1,y1 & x2, y2.
 * Passes optional props 'color' and 'strokeWidth'.
 * 
 * Implementation: No performance boost from using PureComponent because even if the start.x/y & end.x/y are the same, it is likely that new start and end OBJECTS are being passed as props, so it's better to keep the code terse.
 */
const ArrowFromPoints = props => 
	<Arrow
		id={props.id}
		x1={props.start.x}
		y1={props.start.y}
		x2={props.end.x}
		y2={props.end.y}
		color={props.color}
		strokeWidth={props.strokeWidth}
	/>;

ArrowFromPoints.propTypes = {
	id: PropTypes.string.isRequired,
	start: PropTypes.object.isRequired,
	end: PropTypes.object.isRequired,
	color: PropTypes.string,
	strokeWidth: PropTypes.number
};

export default ArrowFromPoints;