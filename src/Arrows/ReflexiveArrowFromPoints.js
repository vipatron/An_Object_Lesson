import React from 'react';
import { PropTypes } from 'prop-types';
import ReflexiveArrow from './ReflexiveArrow.js'

/* ReflexiveArrowFromPoints Functional Component
 * ====================================
 * Composes an ReflexiveArrow Component, allowing you to assign {x, y} Point Objects as props for 'start' and 'end', instead of specifying x1,y1 & x2, y2.
 * Passes optional props 'color' and 'strokeWidth'.
 */
const ReflexiveArrowFromPoints = (props) => {
	return (
		<ReflexiveArrow
			id={props.id}
			x1={props.start.x}
			y1={props.start.y}
			x2={props.end.x}
			y2={props.end.y}
			color={props.color}
			strokeWidth={props.strokeWidth}
		/>
	);
};

ReflexiveArrowFromPoints.propTypes = {
	id: PropTypes.string.isRequired,
	start: PropTypes.object.isRequired,
	end: PropTypes.object.isRequired,
	color: PropTypes.string,
	strokeWidth: PropTypes.number
};

export default ReflexiveArrowFromPoints;