import React from 'react';
import { PropTypes } from 'prop-types';
import CenteredText from './CenteredText';

/* Member Functional Component
 * ===========================
 * Represents a generic member property of a Javascript object. For one relevant to OOJS, use LinkableMember.
 * 
 * IMPLEMENTATION:
 * props.children is the name of the member
 * Returns a rect with class "object_member" in order for the relevant CSS to apply
 * You can optionally specify the dimensions and font size via props and the text will be centered in the rect.
 * Note: There is no checking done to see if the text fits (limitation of SVG)
 */
const Member = props =>
	<g className="object__member" transform={`translate(${props.x}, ${props.y})`}>
		<rect
			className="object__member-rect--own"
			width={props.width}
			height={props.height}
		/>
		<CenteredText
			width={props.width}
			height={props.height}
			classes="object__member-text"
			fontSize={props.fontSize}
		>
			{props.children}
		</CenteredText>
	</g>;

Member.propTypes = {
	children: PropTypes.string.isRequired,
	x: PropTypes.number.isRequired,
	y: PropTypes.number.isRequired,
	width: PropTypes.number,
	height: PropTypes.number,
	fontSize: PropTypes.number
};

Member.defaultProps = {
	width: 174,
	height: 30,
	fontSize: 12
}

export default Member;
