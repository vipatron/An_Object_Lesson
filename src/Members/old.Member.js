import React from 'react';
import { PropTypes } from 'prop-types';
import './Member.css';

/* Member Functional Component
 * ===========================
 * Represents a generic member property of a Javascript object. For one relevant to OOJS, use LinkableMember.
 * 
 * IMPLEMENTATION:
 * props.children is the name of the member
 * Returns a div with:
 *   class "member" in order for the relevant CSS to apply
 */
const Member = props => <div className="member">{props.children}</div>;

Member.propTypes = {
	children: PropTypes.string.isRequired,
};

export default Member;
