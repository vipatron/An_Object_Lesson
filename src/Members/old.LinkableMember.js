import React from 'react';
import { PropTypes } from 'prop-types';

/* LinkableMember Functional Component
 * ===================================
 * Represents a JS object's member property when it is one of the three relevant to the pseudo-inheritance hierarchy of OOJS: prototype, constructor, and __proto__.
 * 
 * IMPLEMENTATION:
 * props.children is the name of the member
 * Returns a div with:
 *   id in spinal case of `${props.parentObjectName}-${props.children}`
 *   class "member" in order for the relevant CSS to apply
 *   a class of "draggable" to be the baseline for string changes during the dragstart and dragend handlers
 *   onDragStart and onDragEnd handlers to implement those changes and modify the state of the app to create an arrow.
 */
const LinkableMember = props => 
	<div 
		id={`${props.parentObjectName}-${props.children}`}
		className={`member draggable ${props.isProtoChainRoot?" proto-chain-root":""}`} 
		draggable
		onDragStart={props.dragStartHandler}
		onDragEnd={props.dragEndHandler}
	>
		{props.children}
	</div>;

LinkableMember.propTypes = {
	children: PropTypes.string.isRequired,
	isProtoChainRoot: PropTypes.bool,
	parentObjectName: PropTypes.string.isRequired,
	dragStartHandler: PropTypes.func.isRequired,
	dragEndHandler: PropTypes.func.isRequired
};

export default LinkableMember;