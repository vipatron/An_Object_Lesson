import React from 'react';
import { PropTypes } from 'prop-types';
import CenteredText from './CenteredText';

/* LinkableMember Functional Component
 * ===================================
 * Represents a JS object's member property when it is one of the three relevant to the pseudo-inheritance hierarchy of OOJS: prototype, constructor, and __proto__.
 * 
 * IMPLEMENTATION:
 * props.children is the name of the member
 * returns a group with:
 *   id in spinal case of `${props.parentObjectName}-${props.children}`
 *   class "object__member--linkable" in order for the relevant CSS to apply
 *   containing:
 *     rect that actually captures the mouse events (since a group can't) and renders in color provided by props.rectClass (red|yellow|green)
 *     CenteredText component that displays the name of the member.
 *   You can optionally specify the dimensions and font size via props and the text will be centered in the rect.
 *   a class of "draggable" to be the baseline for string changes during the dragstart and dragend handlers
 * 
 - TODO: check whether touch Events work correctly.
 / TODO: PropTypes for props.dragHandler (x3 - hereV, ObjectRepresentation, App indentation in render()
 */

function LinkableMember(props){
console.log("linkMember rendered");
	return <g
		id={`${props.parentObjectName}-${props.children}`}
		className="object__member--linkable"
		transform={`translate(${props.x}, ${props.y})`}
		onMouseDown={props.isProtoChainRoot ? null : props.dragStartHandler}
		onTouchStart={props.isProtoChainRoot ? null : props.dragStartHandler}
	>
		<rect
			className={ (props.rectClass || "object__member-rect--draggable") +  (props.isProtoChainRoot?" proto-chain-root__rect":"") }
			width={props.width}
			height={props.height}
		/>
		<CenteredText
			width={props.width}
			height={props.height}
			classes={`object__member-text`}
			fontSize={props.fontSize}
		>
			{props.children}
		</CenteredText>
	</g>;
}

LinkableMember.propTypes = {
	//content
	children: PropTypes.string.isRequired,
	//different from plain Member:
	isProtoChainRoot: PropTypes.bool,
	parentObjectName: PropTypes.string.isRequired,
	dragStartHandler: PropTypes.func,//not required b/c the "ghost copy" that we render to help visualize the drag event shouldn't itself respond to drag events.

	//presentation
	x: PropTypes.number.isRequired,
	y: PropTypes.number.isRequired,
	width: PropTypes.number,
	height: PropTypes.number,
	fontSize: PropTypes.number,
	rectClass: PropTypes.string,
};

LinkableMember.defaultProps = {
	width: 174,
	height: 30,
	fontSize: 14,
}

export default LinkableMember;