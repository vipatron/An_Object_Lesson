import React, { Component } from 'react';
import { PropTypes } from 'prop-types';
import CenteredText from './CenteredText';

/* LinkableMember Stateful Class Component
 * =======================================
 * Represents a JS object's member property when it is one of the three relevant to the pseudo-inheritance hierarchy of OOJS: prototype, constructor, and __proto__.
 * 
 * IMPLEMENTATION:
 * props.children is the name of the member
 * Returns a rect with:
 *   id in spinal case of `${props.parentObjectName}-${props.children}`
 *   class "object_member" in order for the relevant CSS to apply
 *   You can optionally specify the dimensions and font size via props and the text will be centered in the rect.
 *   Note: There is no checking done to see if the text fits (limitation of SVG)
 *   a class of "draggable" to be the baseline for string changes during the dragstart and dragend handlers
 * 
 *   TODO: onDragStart and onDragEnd handlers to implement those changes and modify the state of the app to create an arrow.
 * + TODO: add REAL event handler functionality (?keep draggable or use class)
 * + TODO: in App/ObjectRepresentation constructor add the width sizing functionality currently up in SVGComponentTester.handleClick()
 * - TODO: figure out where in the hierarchy is the correct place to put this CSS.
 */

class LinkableMember extends Component {
	constructor(props){
		super(props);
		this.state = {
			selectedElement: null,
			offset: null,
			transform: null
		}
		this.groupRef = React.createRef();
	}

	componentDidMount(){ //TODO: (Maybe) Drop/AllowDrop - possibly belongs on ObjectRepresentation code.
		console.log("current:", this.groupRef.current.firstChild.id);
		const grp = this.groupRef.current;
		grp.addEventListener('mousedown', this.props.dragStartHandler);
		grp.addEventListener('mousemove', this.props.dragHandler);
		grp.addEventListener('mouseup', this.props.dragEndHandler);
		grp.addEventListener('mouseleave', this.props.dragEndHandler);
		//mobile/touch event listeners
//TODO: clean up the touch events below.
		// grp.addEventListener('touchstart', this.props.dragStartHandler);
		// grp.addEventListener('touchmove', this.props.dragHandler);
		// grp.addEventListener('touchend', this.props.dragEndHandler);
		// grp.addEventListener('touchleave', this.props.dragEndHandler);
		// grp.addEventListener('touchcancel', this.props.dragEndHandler);
	}
	
	componentWillUnmount(){
		const grp = this.groupRef.current;
		grp.removeEventListener('mousedown', this.props.dragStartHandler);
		grp.removeEventListener('mousemove', this.props.dragHandler);
		grp.removeEventListener('mouseup', this.props.dragEndHandler);
		grp.removeEventListener('mouseleave', this.props.dragEndHandler);
		// //mobile/touch event listeners
		// grp.removeEventListener('touchstart', this.props.dragStartHandler);
		// grp.removeEventListener('touchmove', this.props.dragHandler);
		// grp.removeEventListener('touchend', this.props.dragEndHandler);
		// grp.removeEventListener('touchleave', this.props.dragEndHandler);
		// grp.removeEventListener('touchcancel', this.props.dragEndHandler);
	}

	render (){
		return <g
			className="object__member linkable"
			transform={`translate(${this.props.x}, ${this.props.y})`}
			ref={this.groupRef}
			// onMouseDown={evt=>console.log("mouseDown:", evt.target.id)}
		>
			<rect
				id={`${this.props.parentObjectName}-${this.props.children}`}
				className={`object__member-rect--draggable ${this.props.isProtoChainRoot?" proto-chain-root__rect":""}`}
				width={this.props.width}
				height={this.props.height}
		// onClick={evt=>{
		// 	let {top, bottom, left, right} = evt.target.getBoundingClientRect();
		// 	console.log({top, bottom, left, right});
		// }}

		// onMouseEnter={evt=>console.log("entered", evt.target.id)}
		// onMouseLeave={evt=>console.log("left", evt.target.id)}

		// onDragStart={evt=>console.log("drag start:", evt.target.id)}

		// onDragStart={this.props.dragStartHandler}
		// onDragEnd={this.props.dragEndHandler}
			/>
			<CenteredText
				width={this.props.width}
				height={this.props.height}
				// classes={`object__member-text ${this.props.isProtoChainRoot?" proto-chain-root__text":""}`}
				classes={`object__member-text`}
				fontSize={this.props.fontSize}
			>
			{/*TODO: remover the asterisk below, the equivalent of orange color */}
				{this.props.children + (this.props.isProtoChainRoot?"*":"")}
			</CenteredText>
			{/* <div 
				draggable
			>
				{this.props.children}
			</div> */}
		</g>;
	}
}

LinkableMember.propTypes = {
	//content
	children: PropTypes.string.isRequired,
	//new:
	isProtoChainRoot: PropTypes.bool,
	parentObjectName: PropTypes.string.isRequired,
	dragStartHandler: PropTypes.func.isRequired,
	dragEndHandler: PropTypes.func.isRequired,
	//presentation
	x: PropTypes.number.isRequired,
	y: PropTypes.number.isRequired,
	width: PropTypes.number,
	height: PropTypes.number,
	fontSize: PropTypes.number
};

LinkableMember.defaultProps = {
	width: 174,
	height: 30,
	fontSize: 14
}

export default LinkableMember;