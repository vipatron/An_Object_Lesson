import React, { PureComponent } from 'react';
import { PropTypes } from 'prop-types';

/* CenteredText class PureComponent
 * =================================
 * Represents a block of text centered within a rectangular space
 * By implementing a pure component, saves a lot of unnecessary renders for text that is unlikely to change.
 * 
 * PROPS:
 * (x,y) is the upper-left corner of the rectangular space: (0,0) unless passed as props.
 * (x+width, y+height) is the bottom-right corner.
 * children is the text displayed.
 * classes is an optional space-delimited string of classes to apply to the text element.
 * 
 * USAGE NOTES:
 * fontSize is assumed to be 16 unless passed as props.
 * The code makes some assumptions about fonts that are true for Segoe UI that may not be true for other projects. Pass in a different props.fractionalLineSpacing if needed.
 */

 class CenteredText extends PureComponent {
	render(){
		return <text
			dx={this.props.x}
			dy={this.props.y}
			x={this.props.width/2}
			y={(this.props.height + this.props.fontSize - this.props.fontSize*this.props.fractionalLineSpacing)/2 }
			textAnchor='middle'
			fontSize={this.props.fontSize}
			className={this.props.classes}
		>
			{this.props.children}
		</text>;
	} 
 }

CenteredText.propTypes = {
	// children: PropTypes.string.isRequired,
	x: PropTypes.number,
	y: PropTypes.number,
	width: PropTypes.number.isRequired,
	height: PropTypes.number.isRequired,
	fontSize: PropTypes.number,
	fractionalLineSpacing: PropTypes.number,
	classes: PropTypes.string,
}

CenteredText.defaultProps = {
	fontSize: 10,
	fractionalLineSpacing: 0.3, //70% of line height is glyph, 30% spacing on top
}

export default CenteredText;