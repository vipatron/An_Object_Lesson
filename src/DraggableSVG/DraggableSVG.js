import React, { Component } from 'react';
import PropTypes from 'prop-types';
import './DraggableSVG.css';
import getScrollbarWidth from '../utilities/scrollbarWidth';

/**
 * DraggableSVG Component renders a window-sized SVG with a no-scaling, no-offset viewbox.
 * @classdesc allows the dragging of top-level children whose CSS class is in props.classesForDragging[]
 * 
 * NOTE: If props.allowOverlap is false, but the initial draggable elements start in overlapping positions, they won't be made transparent. It is left to the calling function to ensure that the UI is still usable. What will happen when an overlapping box is moved is that it will be moved to the nearest non-overlapping position.
 * 
 + TODONE: Prevent dragging one child "through" another with an aggressive mouse movement. Update collisionDirectionAndPosition to check intendedTranslation.x/y against this.state.transform.matrix.e/f and add Δx/y to box's (right|left / bottom|top) depending on whether the Δ was +/-. [Ex: if (Δ>0) {right += Δ } else {left += Δ}]. Although, right now, this "bug" is really a feature supporting user intention.
 *
 * TODO: add attribute for level of parent to respond to mouseDown. (Right now, assumes event-receiving SVGElement is inside a group whose id we will us to address it and all its siblings and children)
 * 
 * MAYBE:
 * 
 ? MAYBE: try implementing translation as a direct update to state/rerender?
 ? MAYBE: if (!allowOverlap), call makeOverlappingBoxesTransparent() until all boxes are not transparent. CONS: requires some code bloat, with something like this.state.someBoxesOverlap added.
 ? MAYBE: Subclass by Composing App-specific DraggableSVG over? How would we change the rendered "pure" DraggableSVG's <svg> without some conditional logic to override the assignment of functions to SyntheticEvents? Maybe some general "switch", boolean or otherwise, (like dragData.id?) to allow alternate drag handlers? (rename svg to dragSideEffectHandler?)
 */
class DraggableSVG extends Component{
	constructor(props){
		super(props);
		this.svgRef = React.createRef();
		/**
		 * State object
		 * @prop {object} selectedElement - The element that is currently being dragged
		 * @prop {object} offset - {x, y} of offset of mousedown from {left, top} in SVG viewbox coordinates
		 * @prop {object} transform - The first transform in the SVGTransformList of the selectedElement, guaranteed to be a translation.
		 * @prop {object[]} boxes - {top, left, bottom, right, elem} objects that describe the integer extents of the draggable objects between drag events, as well as a reference to each (elem)
		 * @prop {Number} width - width attribute of the rendered SVG.
		 * @prop {Number} height - height attribute of the rendered SVG.
		 */
		this.state = {
			selectedElement: null,
			offset: null,
			transform: null,
			boxes: null,
			width: this.props.width,
			height: this.props.height,
		};

		this.adjustSVGdimensions = this.adjustSVGdimensions.bind(this);
		this.scrollHandler = this.scrollHandler.bind(this);
		this.translateSVGChildren = this.translateSVGChildren.bind(this);
		this.getTranslationTransform = this.getTranslationTransform.bind(this);
		this.makeOverlappingBoxesTransparent = this.makeOverlappingBoxesTransparent.bind(this);
		this.startDrag = this.startDrag.bind(this);
		this.drag = this.drag.bind(this);
		this.endDrag = this.endDrag.bind(this);
	}


	/**
	 * LIFECYCLE AND EVENT HANDLER METHODS:
	 * On mounting, we have to registerEventListeners, update the boxes' pre-drag locations held in state, and
	 * On unmounting, we have to unregister the event listeners.
	 */

	componentDidMount(){
		window.addEventListener('resize', this.adjustSVGdimensions);
		window.onscroll=this.scrollHandler;
		this.updateBoxes();//this lifecycle runs after first render, so state can now be updated with the bounding boxes.
		if (this.props.allowOverlap){
			window.setTimeout(this.makeOverlappingBoxesTransparent, 0);//Must execute after this lifecycle method, because makeOverlappingBoxesTransparent requires that the state update inside of updateBoxes() has executed already.
		}
		this.adjustSVGdimensions(); //in case the SVG elements exceed the window's clientArea, this call resizes the svg from its defaults of: "100%"x(innerHeight-4)
	}
	
	componentWillUnmount(){
		window.removeEventListener('resize', this.adjustSVGdimensions);
	}

	/**
	 * METHODS TO DEAL WITH RESIZING THE SVG:
	 * adjustSVGdimensions()
	 * scrollHandler()
	 * translateSVGchildren()
	 * getTranslationTransform(svgChild) - also called by startDrag.
	 */

	componentDidUpdate(prevProps){
		//check to see whether the number of direct children, or children in arrays has changed, which may necessitate more or less canvas area. In that case, resize.
		const countNonArrayChildren = (numChildren, elem) => numChildren + (Array.isArray(elem) ? elem.length : (elem ? 1 : 0));
		if (this.props.children.reduce( countNonArrayChildren, 0 ) !== prevProps.children.reduce(countNonArrayChildren, 0)){
			// setTimeout(this.adjustSVGdimensions, 0);
			this.adjustSVGdimensions();
			this.updateBoxes();
		}
	}

	/**
	 * @method adjustSVGdimensions
	 * @description adjusts the size of the svg to get each dimension to fit into 1 of 3 sizes: minimum necessary, the clientArea, or clientArea - scrollbar width.
	 */
	adjustSVGdimensions(){
		/* ALGO:
		 * 1. Calculate neededDimensions = (rightMost, bottomMost) + (padding, padding) + (scrollX, scrollY) 
		 * 2. Let innerDimensions = (window.innerWidth-1, window.innerHeight-5) Note: why it's no longer (-0,4), I can't say. Experiments show that setting it 4 only works on increasing window height, but not decreasing, causing scrollbars where we don't want them.
		 * 3. Check whether neededDimensions fits inside (<=) innerDimensions. If so, no scrollbars, no problem: set the svg to innerDimensions. You'll get a scroll-bar free, completely contained background.
		 * 4. Check whether neededDimensions > innerDimensions. If so, both axes have scrollBars, and you can just set svg to: neededDimensions. since scrollDimensions < innerDimensions, there is no way that the svg could fit inside either one, so again, no problem.
		 * 5. If ONE neededDimension is bigger than innerDimensions (overflows), it is a little more challenging, since the OTHER axis will be shortened by a scrollbar's width/height.
		 * 6. Let scrollDimensions = inner - (the width of a scrollbar, calculated for any browser via memoized fxn call)
		 * 7. If OTHER dimension needs <= scrollDimension.OTHER, set svg.ONE = needed, svg.OTHER = 100%/scroll.height (if other == W/H).
		 * 8. If OTHER dimension needs > scrollDimension[OTHER], then you're going to need to scroll along that OTHER dimension, shortening dimension ONE by a scrollbar, which won't matter because dimension ONE was already larger than innerDimension, so set svg to: needed, needed.
		 */
		/* 1: find the extreme (x,y) values (in window coordinates)
		 *Note: if somehow, the grandchild or more distant descendants of the SVG somehow exceed the bounds of the top level children's clientRect, this algorithm won't work and a full tree traversal may be necessary.
		 * Impt note: since getBoundingClientRects() accounts for any scrolling, the values in the returned DOMRect are relative to an origin which is the (left, top) of the window's viewport, same as window.innerW/H
		 */
		let [rightMost, bottomMost] = [Number.NEGATIVE_INFINITY, Number.NEGATIVE_INFINITY];
		const topLevelSVGElements = this.svgRef.current.childNodes;
		topLevelSVGElements.forEach(elem => {
			const {right, bottom} = elem.getBoundingClientRect();
			rightMost = Math.max(right, rightMost);
			bottomMost = Math.max(bottom, bottomMost);
		});
		const needed = {
			width: Math.ceil(rightMost + this.props.padding + window.scrollX),
			height: Math.ceil(bottomMost + this.props.padding + window.scrollY)
		}
		//2+3: decide whether scrollbars are unnecessary.
		const inner = {
			width: window.innerWidth -1,
			height: window.innerHeight-5
		};
		let newDimensions;
		if (needed.width <= inner.width && needed.height <= inner.height){//3: no scrollbars needed
			newDimensions = inner;
		} else if (needed.width > inner.width && needed.height > inner.height){//4: both scrollbars needed
			newDimensions = needed;
		} else {//5: ONE dimension is larger than window. must test whether svg fits inside scrollbarWidth
			const scroll = {//6: define scrollDimensions
				width: inner.width - getScrollbarWidth(),
				height: inner.height - getScrollbarWidth()
			};
			let overflowDimension, otherDimension;
			if (needed.width > inner.width){
				overflowDimension = "width";
				otherDimension = "height";
			} else {//one dimension was guaranteed to overflow. If not width, then height.
				overflowDimension = "height";
				otherDimension = "width";
			}
			if (needed[otherDimension] <= scroll[otherDimension]){//7: non-overflow dimension fits inside scrollbar
				newDimensions = {};
				newDimensions[overflowDimension] = needed[overflowDimension];
				newDimensions[otherDimension] = otherDimension === "width" ? "100%" : scroll.height;
			} else { //8:non-overflow dimension doesn't fit inside scrollbar.
				newDimensions = needed;
			}
		}
		this.setState(newDimensions);
	}
	
	/**
	 * @method scrollHandler
	 * @description crops the SVG down if there is blank space off-screen (only really needed to handle window scrolling)
	 */
	scrollHandler(){
		/* ALGORITHM:
		 * If svg.(left, top) is off screen (<0) AND (leftMost, topMost) - (padding, padding) is onscreen, we should shorten the svg:
		 * first - adjust the transforms of the kids.
		 * second - update state.width/height. (Note: we don't have to account for a percentage string b/c the only way to get a scrollbar was to set a fixed (numerical) width or height.)
		 */
		const svgRect = this.svgRef.current.getBoundingClientRect();
		let [leftMost, topMost] = [Infinity, Infinity];
		const topLevelSVGElements = this.svgRef.current.childNodes;
		topLevelSVGElements.forEach(elem => {
			const {left, top} = elem.getBoundingClientRect();
			leftMost = Math.min(left, leftMost);
			topMost = Math.min(top, topMost);
		});
		//adjust (leftMost, topMost) by (-padding, -padding) to ensure we don't overzealously crop the svg.
		leftMost -= this.props.padding;
		topMost -= this.props.padding;
		//get the integer adjustment amount. We only want to cut off the "excess" svg area that is to the left/top of the clientArea, and only for the dimensions which have excess.
		let dx = 0, dy = 0;
		const newDimensions = {};
		if (svgRect.left < 0 && leftMost >= 0){
			dx = Math.ceil(svgRect.left);
			newDimensions.width = this.state.width + dx;
		}
		if (svgRect.top < 0 && topMost >= 0){
			dy = Math.ceil(svgRect.top);
			newDimensions.height = this.state.height + dy;
		}
		if (dx || dy){//need to adjust svg if either is non-zero (positive by virtue of the conditions on svgRect.left/top).
			this.translateSVGChildren(dx,dy);
			this.setState(newDimensions);
		}
	}

	/**
	 * @method translateSVGChildren
	 * @description Utility method that translates every child of the svg
	 * @param  {Number} dx - an integer by which to translate all svg children "to the right"
	 * @param  {Number} dy - an integer by which to translate all svg children "down"
	 */
	translateSVGChildren(dx, dy){
		const topLevelSVGElements = this.svgRef.current.childNodes;
		topLevelSVGElements.forEach(child => {
			if (this.props.classesExcludedFromResizeTranslation && !this.props.classesExcludedFromResizeTranslation.some(
				classString => child.classList.contains(classString)
			)){
				const translate = this.getTranslationTransform(child);
				translate.setTranslate(translate.matrix.e + dx, translate.matrix.f + dy);
			} else {
				this.props.svgChildDragHandler(); //allows the calling component to update DraggableSVG's props.children if necessary.
			}
		});
	}

	/**
	 * @method getTranslationTransform
	 * @description Utility function that determines whether the first transform in svgChild.transform.baseVal is a translation transform, and if not, inserts one and returns a reference to that transform.
	 * @param  {Object} svgChild
	 * @returns {Object} SVGTransform object representing a translation
	 */
	getTranslationTransform(svgChild){
		const childTransformList = svgChild.transform.baseVal;
		if (childTransformList.length === 0 || childTransformList.getItem(0).type !== SVGTransform.SVG_TRANSFORM_TRANSLATE){
			const newTranslate = this.svgRef.current.createSVGTransform();
			newTranslate.setTranslate(0, 0);
			childTransformList.insertItemBefore(newTranslate, 0);
		}
		return childTransformList.getItem(0);
	}

	/**
	 * METHODS TO DEAL WITH DRAGGING SVG CHILDREN AROUND:
	 * updateBoxes()
	 * elemToIntegerBox(elem)
	 * 
	 * startDrag(evt)
	 * getMousePosition(evt)
	 * viewboxCoordinates(clientX, clientY)
	 * 
	 * drag(evt)
	 * makeOverlappingBoxesTransparent(draggedElem)
	 * getTranslationOfFirstCollision(draggedElem, intendedTranslation)
	 * 
	 * endDrag()
	 */

	/**
	 * @method updateBoxes
	 * @description updates the state with a box object for each draggable element.
	 */
	updateBoxes(){
		const boxes = {};
		if (this.props.classesForDragging){
			this.svgRef.current.childNodes.forEach( child => {
				if (this.props.classesForDragging.some(
					classString => child.classList.contains(classString)
				)){
					boxes[child.id] = this.elemToIntegerBox(child);
					boxes[child.id].elem = child;
				}
			});
		}
		this.setState({ boxes });
	}

	/**
	 * @method elemToIntegerBox
	 * @description gets the bounding box whose values are all integers (>= area of DOMrect), in the SVG's viewbox coordinates
	 * @param {Object} elem - the element whose bounding box will be returned
	 * @returns {Object} {top, left, bottom, rect} in SVG viewbox coordinates.
	 */
	elemToIntegerBox(elem){
		//intendedTranslation is in SVG coordinates. We need to get left, top, right, bottom in SVG coordinates to do arithmetic together.
		const DOMRect = elem.getBoundingClientRect();
		const {x: left, y: top} = this.viewboxCoordinates(DOMRect.left, DOMRect.top);
		const {x: right, y: bottom} = this.viewboxCoordinates(DOMRect.right, DOMRect.bottom);
		return {
			left: Math.floor(left),
			top: Math.floor(top),
			right: Math.ceil(right),
			bottom: Math.ceil(bottom)
		};
	}

	/**
	 * @method startDrag
	 * @description Begins the dragging process, putting the selected element, mouse offset, and pointer to the translation transform that describes the progress of the drag
	 * @param  {Object} evt - the event object passed by the DOM
	 */
	startDrag(evt){
		if (this.props.classesForDragging.some(classString =>
			evt.target.parentNode.classList.contains(classString)
		)){
			const selectedElement = evt.target.parentNode;

			const offset = this.getMousePosition(evt); //get the "viewbox" coordinates of the mouse in the total SVG.

			//Get initial translation amount (a ref, not a copy, so changes immediately affect the list, which is what drag() will do.)
			const transform = this.getTranslationTransform(selectedElement);//either (0,0) or some other prior transformation.
			//update the coordinates of the mouse by the first translation placed on the selectedElement (0,0) if the drag is the only transform we're applying.
			// In essence, we're subtracting selectedElement's(left, top) from mouse(x,y) [in viewbox coordinates] to make offset equal the coordinates of the mouse INSIDE the element.
			//shorthand: offset = mousePosition - translation
			offset.x -= transform.matrix.e;
			offset.y -= transform.matrix.f;
			this.setState({ selectedElement, offset, transform });
		}
	}
	
	/**
	 * @method getMousePosition
	 * @description returns the viewbox coordinates of the mouse pointer or the first touch of a multitouch input event
	 * @param  {Object} evt - the event object passed by the DOM
	 */
	getMousePosition(evt){
		if (evt.touches) evt = evt.touches[0];
		return this.viewboxCoordinates(evt.clientX, evt.clientY);
	}

	/**
	 * @method viewboxCoordinates
	 * @description (X,Y) in client coordinates will return correct SVG viewbox coordinates
	 * @param  {Number} clientX
	 * @param  {Number} clientY
	 * @returns {Object} - {x, y} in SVG viewbox coordinates
	 */
	viewboxCoordinates(clientX, clientY){
		const CTM = this.svgRef.current.getScreenCTM();
		return {
			// Human Translation of below:
			//(DOM.x - viewbox.Offset)/scaling factor of screen/viewbox rect size.
			x: (clientX - CTM.e) / CTM.a,
			y: (clientY - CTM.f) / CTM.d
		};
	}
	
	/**
	 * @method drag
	 * @description deals with the ongoing drag process, making boxes transparent or performing edge detection and updating the mouse offset in state per props.allowOverlap
	 * @param  {Object} evt - the event object passed by the DOM
	 */
	drag(evt){
		if (this.state.selectedElement){
			evt.preventDefault();
			const coord = this.getMousePosition(evt);
			/** Notes:
			 * offset is the mouse position inside the element (viewbox coordinates)
			 * coord is the mouse position inside the SVG (viewbox coordinates)
			 * Therefore, (coord - offset) is the place we want (left, top) to be if there's no problem with moving selectedElement there.
			 * We need to account for the possibility that one or more of those directions is blocked (if not allowing overlap).
			 * If a direction is blocked, we need to update the offset using the new translation coordinates returned from getTranslationOfFirstCollision()
			 */
			const newTranslation = {
				x: coord.x - this.state.offset.x,
				y: coord.y - this.state.offset.y
			};
			if (this.props.allowOverlap){
				this.makeOverlappingBoxesTransparent();
			} else {
				const newState = {};
				const noncollidingTranslation = this.getTranslationOfFirstCollision(this.state.selectedElement, newTranslation);
				
				if (noncollidingTranslation !== newTranslation){
					// If intended translation would have resulted in a collision, update the new translation transform
					newTranslation.x = noncollidingTranslation.x;
					newTranslation.y = noncollidingTranslation.y;
					const offset = {//the mouse position stayed the same, but the position of the selected Element moved, so we need to update the offset.
						x: coord.x - newTranslation.x,
						y: coord.y - newTranslation.y
					}
					newState.offset = offset;
				}
				this.setState(newState);
			}
			this.state.transform.setTranslate(newTranslation.x, newTranslation.y);
			this.props.svgChildDragHandler();
		}
	}

	/**
	 * @method makeOverlappingBoxesTransparent
	 * @description compare all draggable elements, and if they overlap, make them transparent
	 * @param  {Object} draggedElem
	 */
	makeOverlappingBoxesTransparent(draggedElem){
		const ids = Object.keys(this.state.boxes);
		const boxes = ids.map(id => this.elemToIntegerBox(this.state.boxes[id].elem));//boxes[] contains the element refs themselves, and are indexed the same as ids[]
		const areTransparent = {};
		for (let i=0; i<ids.length; i++){
			for (let j=i+1; j<ids.length; j++){
				//skip comparison if both are already overlapping some object: since  nothing will change that.
				if (areTransparent[ids[i]] && areTransparent[ids[j]]){//in the first loop, some properties of areTransparent{} may be undefined, but every necessary key will be filled in after the first "j" loop. That's fine, since undefined is falsy.
					break;
				}
				const overlap = {//calculate the overlap between opposing edges.
					left: boxes[j].right - boxes[i].left,
					right: boxes[i].right - boxes[j].left,
					top: boxes[j].bottom - boxes[i].top,
					bottom: boxes[i].bottom - boxes[j].top
				};
				let someOverlap = true;
				const edges = ["left", "right", "top", "bottom"];
				for (let e=0; e<edges.length; e++){
					if (overlap[edges[e]] < this.props.unitsOfOverlapNecessaryForTransparency){//no overlap if any negative values (default value: 1)
						someOverlap = false;
						break;
					}
				}
				//if undefined (first j loop) => true/false. If previously false => someOverlap value. If previously true => still true;
				areTransparent[ids[i]] = areTransparent[ids[i]] || someOverlap;
				areTransparent[ids[j]] = areTransparent[ids[j]] || someOverlap;
			}
		}
		for (let id in areTransparent){
			if (areTransparent[id]){
				this.state.boxes[id].elem.classList.add(this.props.classNameForTransparency);
			} else {
				this.state.boxes[id].elem.classList.remove(this.props.classNameForTransparency);
			}
		}
	}

	/**
	 * @method getTranslationOfFirstCollision
	 * @description given an elem and a translation intended by the user, returns the translation of the first collision along that line, which will be the passed intendedTranslation if no collision occurred.
	 * @param  {Object} draggedElem the dragged element
	 * @param  {Object} intendedTranslation user's attempted new position for the element (possibly blocked)
	 * @returns {Object} an Object of shape {x, y} describing the new translation transform to apply to the draggedElem
	 */
	getTranslationOfFirstCollision(draggedElem, intendedTranslation){
		// lookup tables defined to improve code readability and implement directional logic. Defined here, outside the nested forEach calls, for performance reasons, 
		const OPPOSITE = {// returns opposite edge name
			left: "right",
			right: "left",
			top: "bottom",
			bottom: "top"
		};
		const PERPENDICULAR_EDGES = {// returns an array of edge names ordered as [lowerNumericalValue, higherNumericalValue]
			left: ["top", "bottom"],
			right: ["top", "bottom"],
			top: ["left", "right"],
			bottom: ["left", "right"]
		};
		const HORIZONTAL_MOTION = {// keyed by leading edge, returns boolean for whether this being a leading edge results in horizontal motion.
			left: true,
			right: true,
			top: false,
			bottom: false
		};
		const MOVES_TO_POSITIVE_VALUES = {//keyed by leading edge, returns boolean for whether this being a leading edge results in motion toward the positive end of the x or y axis.
			right: true,
			bottom: true,
			left: false,
			top: false,
		};

		//process parameters and state to abstract relevant data or improve readability
		const draggedId = draggedElem.id
		const draggedBox = this.elemToIntegerBox(draggedElem);
		const { boxes } = this.state;
		const previousTranslation = {
			x: this.state.transform.matrix.e,
			y: this.state.transform.matrix.f
		};		
		const dx = intendedTranslation.x - previousTranslation.x;
		const dy = intendedTranslation.y - previousTranslation.y;
		let leadingEdges = [];
		if (dx > 0) leadingEdges.push("right");
		if (dx < 0) leadingEdges.push("left");
		if (dy > 0) leadingEdges.push("bottom");
		if (dy < 0) leadingEdges.push("top");

		//Create an object to store all possible results, keyed by leadingEdge, whose values are arrays which will contain objects of shape {translation:{x, y}, distanceTravelled} by which we can judge which leadingEdge's collision would occur first if the intended mouse movement were allowed to occur to completion.
		const potentialCollisions = leadingEdges.reduce((potentialCollisionsForEdge, edge) => {
			potentialCollisionsForEdge[edge] = [];
			return potentialCollisionsForEdge;
		}, {});
				
		leadingEdges.forEach(leadingEdge => {
			// These calculations improve readability and are done once per leadingEdge for performance reasons.
			const sides = PERPENDICULAR_EDGES[leadingEdge];
			const draggedBoxSize = draggedBox[sides[1]] - draggedBox[sides[0]];
			//calculate the amount by which we want to extend the leadingEdge to either the positive or negative side of its axis to account for all possible positions it could take during the mouse movement. If there is negative motion along that axis, there won't be positive motion and vice-versa. So, by testing an "extended" leadingEdge against each other box's facing edge, we can determine whether a collision between those two faces is possible.
			const perpendicularMotion = HORIZONTAL_MOTION[leadingEdge] ? dy : dx;
			const negativeSideMotion = Math.min(perpendicularMotion, 0);
			const positiveSideMotion = Math.max(perpendicularMotion, 0);

			const boxesThatCanBeHit = Object.keys(boxes).filter( boxId => //keep those whose facing edges are (in the correct quadrant && have horizontal|vertical coordinates in range of dx|dy, respectively).
				// IMPLEMENTATION note: because elemToIntegerBox() rounds up decimal values from Element.getBoundingClientRect(), it is possible that two adjacent elements could be have box[edge] values that overlap. Thus, we have to check 1 more SVG unit than the OPPOSITE[leadingEdge] to ensure that we get all possible collisions.
				boxId !== draggedId && 
				( MOVES_TO_POSITIVE_VALUES[leadingEdge]
					? draggedBox[leadingEdge] <= boxes[boxId][OPPOSITE[leadingEdge]] + 1 &&
						draggedBox[leadingEdge] + (HORIZONTAL_MOTION[leadingEdge] ? dx : dy) >= boxes[boxId][OPPOSITE[leadingEdge]]
					: draggedBox[leadingEdge] + 1 >= boxes[boxId][OPPOSITE[leadingEdge]] &&
					draggedBox[leadingEdge] + (HORIZONTAL_MOTION[leadingEdge] ? dx : dy) <= boxes[boxId][OPPOSITE[leadingEdge]]
				)
			).filter( boxId => {//keep those boxes whose facing edges intersect with "extended" leadingEdge when projected onto leadingEdge's axis.
				let boxCanBeHit = false;
				const stationaryBoxSize = boxes[boxId][sides[1]] - boxes[boxId][sides[0]];
				if (stationaryBoxSize >= draggedBoxSize){//handles case where stationary box contains draggedBox along relevant axis.
					boxCanBeHit = boxes[boxId][sides[0]] <= draggedBox[sides[0]] && draggedBox[sides[1]] <= boxes[boxId][sides[1]];
				}
				boxCanBeHit = boxCanBeHit || sides.some(sideToTestForOverlap =>
					//returns true if box overlaps draggedBox's extended range during the motion or box's extent along leadingEdge's axis is contained inside draggedBox
					negativeSideMotion + draggedBox[sides[0]] < boxes[boxId][sideToTestForOverlap] &&
					boxes[boxId][sideToTestForOverlap] < draggedBox[sides[1]] + positiveSideMotion
				);
				return boxCanBeHit;
			});
			// at this point in the code, boxesThatCanBeHit only contains the boxIds of elements that WILL be hit by dy or dx (or both) if the mouse movement isn't truncated and overlapping were allowed.
			boxesThatCanBeHit.forEach(boxId => {
				const distanceBetweenFaces = boxes[boxId][OPPOSITE[leadingEdge]] - draggedBox[leadingEdge];

				const potentialCollision = {blockedEdge: leadingEdge};
				// if (leadingEdges.length==1){use the distance between the faces} else {the hypoteneusal distance}
				if (leadingEdges.length === 1){
					potentialCollision.distanceTravelled = Math.abs(distanceBetweenFaces);
					potentialCollision.translation = {
						x: previousTranslation.x + (HORIZONTAL_MOTION[leadingEdge] ? distanceBetweenFaces: 0),
						y: previousTranslation.y + (HORIZONTAL_MOTION[leadingEdge] ? 0 : distanceBetweenFaces)
					}
				} else {
					const slope = dy/dx;
					//the distanceBetweenFaces is one leg of a right triangle describing the prematurely-arrested mouse movement. We can use the slope to calculate the otherLeg (similar triangles), and then use that to calculate the hypoteneuse, which will be the distance travelled until the point of collision.
					const otherLeg = HORIZONTAL_MOTION[leadingEdge]
						? distanceBetweenFaces * slope //dy = dx * slope = dx (dy/dx)
						: distanceBetweenFaces / slope; //dx = dy / slope = dy / (dy/dx)
					const hypoteneuse = Math.sqrt(distanceBetweenFaces**2 + otherLeg**2);
					// We want to AVOID a collision (i.e.: prevent overlap), so we must round the otherLeg(probably a float) down to the nearest integer.
					const integerOtherLeg = otherLeg > 0 ? Math.floor(otherLeg) : Math.ceil(otherLeg);

					potentialCollision.distanceTravelled = hypoteneuse;
					potentialCollision.translation = {
						x: previousTranslation.x + (HORIZONTAL_MOTION[leadingEdge] ? distanceBetweenFaces: integerOtherLeg),
						y: previousTranslation.y + (HORIZONTAL_MOTION[leadingEdge] ? integerOtherLeg : distanceBetweenFaces)
					}
				}
				potentialCollisions[leadingEdge].push(potentialCollision);
			});
		});
		//return the translation of the first collision which would occur among all edges. If no edge collided, then by default we return the intended translation.
		let firstCollision = {
			distanceTravelled: Infinity,
			translation: intendedTranslation
		};
		for (let edge in potentialCollisions){
			firstCollision = potentialCollisions[edge].reduce((closestCollision, currentCollision) => 
				currentCollision.distanceTravelled < closestCollision.distanceTravelled ? currentCollision : closestCollision
				, firstCollision
			);
		}
		if (firstCollision.distanceTravelled === 0 && leadingEdges.length === 2){
			//check to make sure that we weren't blocked in more than one direction.
			const bothDirectionsBlocked = Object.keys(potentialCollisions).every(
				edge => potentialCollisions[edge].some(collision => collision.distanceTravelled === 0)
			);
			if (!bothDirectionsBlocked){//call recursively (one direction of motion is a base case)
				const slidingPartOfIntendedTranslation = {
					x: HORIZONTAL_MOTION[firstCollision.blockedEdge] ? previousTranslation.x : intendedTranslation.x,
					y: HORIZONTAL_MOTION[firstCollision.blockedEdge] ? intendedTranslation.y : previousTranslation.y
				};
				return this.getTranslationOfFirstCollision(draggedElem, slidingPartOfIntendedTranslation);
			}
		}
		return firstCollision.translation;
	}

	/**
	 * @method endDrag
	 * @description ends the drag, updating the fixed positions of the boxes and resetting the elements of state that track an active drag to null.
	 */
	endDrag(){
		if (this.state.selectedElement) {
			this.updateBoxes();
			/* SVG resizing/repositioning ALGORITHM:
			 * let (dx, dy) = (padding, padding) - new position's (left, top).
			 * We'll have to translate every top-level child of SVG by (+dx, +dy) if necessary, (if dx > 0, i.e.: if the dragged elem was positioned "more towards the negative end of an axis" than padding would allow, making dx/y positive);
			 * Iterate over the kids: ensure the first transform is a translate and then adjust it by (dx, dy).
			 * check if the svg needs to be resized with a call to adjustSVGdimensions()
			 * scroll the page by (dx, dy).
			 */
			let {e: left, f: top} = this.state.transform.matrix;
			const dx = Math.max(0, this.props.padding - left);
			const dy = Math.max(0, this.props.padding - top);
			this.translateSVGChildren(dx, dy);
			this.adjustSVGdimensions();
			window.setTimeout(
				() => window.scrollTo({
					left: window.scrollX + dx,
					top: window.scrollY + dy,
					behavior: 'auto'
				}),
				0
			);
			this.setState({
				selectedElement: null,
				transform: null,
				offset: null,
			});
		}
	}
	
	render(){
		if (this.props.makeTransparentIfCollidingDragResultsInOverlap) setTimeout(this.makeOverlappingBoxesTransparent, 0);
		return <svg
			ref={this.svgRef}
			height={this.state.height}
			width={this.state.width}
			onMouseDown={this.startDrag}
			onMouseMove={this.props.dragData.id ? this.props.memberDragHandler : this.drag}
			onMouseUp={this.props.dragData.id ? this.props.memberDragEndHandler: this.endDrag}
			onMouseLeave={this.props.dragData.id ? this.props.memberDragEndHandler: this.endDrag}
			onTouchStart={this.startDrag}
			onTouchMove={this.props.dragData.id ? this.props.memberDragHandler : this.drag}
			onTouchEnd={this.props.dragData.id ? this.props.memberDragEndHandler: this.endDrag}
			onTouchCancel={this.props.dragData.id ? this.props.memberDragEndHandler: this.endDrag}
			className={this.props.dragData.id ? "background__cursor--no-drop" : ""}
		>
			{this.props.children}
		</svg>
	}
};

function dimensionPropTypeValidator(props, dimension){
	if ( !(typeof props[dimension] == "number" || 
		(typeof props[dimension] == "string" && props[dimension].slice(-1) === "%")) ){//if not one of the two cases: a number or a string ending in "%"...
		return new Error('Invalid prop `' + dimension + '` supplied to' +
		' `DraggableSVG`. Must be a Number or a String ending in "%".');
	}
}

DraggableSVG.propTypes = {
	classesForDragging: PropTypes.arrayOf(PropTypes.string),
	width: dimensionPropTypeValidator,
		//Allows numbers and percentage strings. If string passed, must be a percentage string. Note: if percentages are given, they will be of the window's inner dimensions, not of the parent, since this tool is assumed to be full-screen background. MAYBE: implement a flag that lets you select percentages of their containing divs.
	height: dimensionPropTypeValidator,
	allowOverlap: PropTypes.bool,
	makeTransparentIfCollidingDragResultsInOverlap: PropTypes.bool,
	unitsOfOverlapNecessaryForTransparency: PropTypes.number,
	padding: PropTypes.number,//in svg units (px, if no zoom applied).
	classNameForTransparency:PropTypes.string,
	//possibly keep when porting DraggableSVG to other projects: a way to alert parent about events:
	classesExcludedFromResizeTranslation: PropTypes.arrayOf(PropTypes.string),
	svgChildDragHandler: PropTypes.func,
	//implemented only for Objectify App - can be removed for other applications.
	dragData: PropTypes.object,
	memberDragHandler: PropTypes.func,
	memberDragEndHandler: PropTypes.func,
};

DraggableSVG.defaultProps = {
	width: "100%",
	height: window.innerHeight-4,//4 is experimentally-derived: gets rid of vertical scrollbar on Firefox and Chrome.
	allowOverlap: true,
	makeTransparentIfCollidingDragResultsInOverlap: true,
	unitsOfOverlapNecessaryForTransparency: 1,//every rounded unit of border will require roughly 2. DraggableSVG remains agnostic as to CSS. Set to 0, this prop means: touching => transparent, even if allowOverlap is false. The default will occasionally lead to this behavior when allowOverlap is true due to rounding errors. If this is undesirable, set to 2 when you want edge detection to ensure this never happens.
	classNameForTransparency: "transparent",
	padding: 8
};


export default DraggableSVG;