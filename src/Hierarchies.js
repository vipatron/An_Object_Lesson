//This is a datafile containing various inheritance hierarchies.
//TODO: write hierarchies for furniture and People/Employees

const HIERARCHIES = [
	{//Animals
		Animal: {
			isConstructor: true,
			prototype:{
				members:["eat","move", "replicate"],
				constructor: "Object"
			}
		},
		Carnivore: {
			isConstructor: true,
			prototype:{
				members:["kill", "hunt"],
				constructor: "Animal"
			}
		},
		Cat: {
			isConstructor: true,
			prototype:{
				members: ["meow"],
				constructor: "Carnivore"
			}
		},
		"sid The Butt": {
			isConstructor: false,
			constructor: "Cat"
		},
		"Garfield": {
			isConstructor: false,
			constructor: "Cat"
		}
	},
	{//Vehicles
		Vehicle: {
			isConstructor: true,
			prototype:{
				members:["moves","contains"],
				constructor: "Object"
			}
		},
		Plane: {
			isConstructor: true,
			prototype:{
				members:["flies"],
				constructor: "Vehicle"
			}
		},
		//Testing code for right-sided prototype placement.
		// Weatherlight: {
		// 	isConstructor: false,
		// 	constructor: "Vehicle"
		// },
		// Boat: {
		// 	isConstructor: true,
		// 	prototype: {
		// 		members: ["floats"],
		// 		constructor: "Vehicle"
		// 	}
		// },
		Automobile: {
			isConstructor: true,
			prototype:{
				members: ["drives on road"],
				constructor: "Vehicle"
			}
		},
		"Model T": {
			isConstructor: false,
			constructor: "Automobile"
		},
		"HumVee": {
			isConstructor: false,
			constructor: "Automobile"
		},
		"Spirit of Saint Louis":{
			isConstructor: false,
			constructor: "Plane"
		}
	},
	{},
	{},
];

//Note: this list must be maintained by the coder in parallel with the above state description. Unified data requires more computation and coding than it's worth for such a small n.
export const HIERARCHY_LABELS=["Animals", "Vehicles", "Basic1", "Basic2"]

/**
 * @function createObjectHierarchy
 * @description creates state.objects for the prototypal-inheritance App.
 * @param {boolean} isFunctionDisplayed - determines whether the hierarchy includes the Function constructor.
 * @param {Number} hierarchyIndex - the index in the hierarchy array to choose. If too large, it wraps around. If negative, no subclasses of Object will be returned.
 */
const createObjectHierarchy = (isFunctionDisplayed, hierarchyIndex) => Object.assign(
	isFunctionDisplayed
		? {
			Function: {
				isConstructor:true,
				prototype:{
					constructor: "Object"
				}
			}
		}
		: {},
	{
		Object: {
			isConstructor: true,
			prototype:{
				constructor: null
			}
		}
	},
	HIERARCHIES[hierarchyIndex % HIERARCHIES.length]
);

export default createObjectHierarchy;