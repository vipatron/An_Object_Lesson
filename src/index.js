import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';
// import ThisTester from './side_ideas/ThisTester';
// import SetStateTest from './side_ideas/SetStateTest';
import SVGComponentTester from './SVGComponentTest';
// import getScrollbarWidth from './utilities/scrollbarWidth';

// console.clear();
// console.log("scrollbar width:", getScrollbarWidth());

// ReactDOM.render(<App />, document.getElementById('root'));
ReactDOM.render(<SVGComponentTester />, document.getElementById('root'));
// ReactDOM.render(<SetStateTest/>, document.getElementById('root'));
// ReactDOM.render(<ThisTester/>, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister();
