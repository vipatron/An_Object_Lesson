export default function makeDraggable(ref){
	const svg = ref.current;
	svg.addEventListener('mousedown', startDrag);
	svg.addEventListener('mousemove', drag);
	svg.addEventListener('mouseup', endDrag);
	svg.addEventListener('mouseleave', endDrag);
	//mobile/touch event listeners
	svg.addEventListener('touchstart', startDrag);
	svg.addEventListener('touchmove', drag);
	svg.addEventListener('touchend', endDrag);
	svg.addEventListener('touchleave', endDrag);
	svg.addEventListener('touchcancel', endDrag);

	let [selectedElement, offset, transform] = [null, null, null];

	function initializeDragging(evt){
		offset = getMousePosition(evt); //get the "viewbox" coordinates of the mouse in the total SVG.
		console.log("initializeDragging, offset:", offset);

		// Get the baseline values of the attributes (prior to any animated transformations running). i.e.: Get all the transforms currently on this element
		// console.log("selected:", selectedElement);//testing code.
		const transforms = selectedElement.transform.baseVal;
		console.log("initializeDragging, transforms:", transforms);

		//Ensure that the first transform is a translate transform
		if (transforms.length === 0 || transforms.getItem(0).type !== SVGTransform.SVG_TRANSFORM_TRANSLATE){
				//Create a transform that translates by (0, 0) (per MDN, this is outside of any document trees, so that's why we'll have to insert it below. Link: https://developer.mozilla.org/en-US/docs/Web/API/SVGSVGElement#Methods (third from last item in section))
				const translate = svg.createSVGTransform();
				translate.setTranslate(0, 0);

				//Add the translation to the front of the transforms list
				selectedElement.transform.baseVal.insertItemBefore(translate, 0);
		}

		//Get initial translation amount (a ref, not a copy, so changes immediately affect the list, which is what drag() will do.)
		transform = transforms.getItem(0);//either (0,0) or some other prior transformation.
		console.log("initializeDragging, transform:", transform);
		offset.x -= transform.matrix.e;
		offset.y -= transform.matrix.f;
	}
	
	function startDrag(evt) {
		//TODO: determine whether to even keep the first case.(I believe everything we want to drag is in a group with a rect and text)
		if (evt.target.classList.contains('draggable')){
			selectedElement = evt.target;
			initializeDragging(evt);
		} else if (evt.target.parentNode.classList.contains('object')){
			selectedElement = evt.target.parentNode;
			initializeDragging(evt);
		}
	}

	function drag(evt){
		//TODO: edge detection.
		if (selectedElement){
			evt.preventDefault();
			const coord = getMousePosition(evt);
			transform.setTranslate(coord.x - offset.x, coord.y - offset.y);
		}
	}

	function endDrag(evt){
		selectedElement = null;
	}

	function getMousePosition(evt){
		const CTM = svg.getScreenCTM();
		if (evt.touches) evt = evt.touches[0];
		return {
			//Translation of below:
			//(DOM.x - viewbox.Offset)/scaling factor of screen/viewbox rect size.
			x: (evt.clientX - CTM.e) / CTM.a,
			y: (evt.clientY - CTM.f) / CTM.d
		};
	}
}