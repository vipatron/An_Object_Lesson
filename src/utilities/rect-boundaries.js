//TODO: Memoize this function, resetting the cache after a new call to one of the line/point functions?

/* getRect Utility function (not exported)
 * =======================================
 * DRYs out the code for getting the client rectangle for the client rectangle whose id is parameter #id
 * 
 * RETURNS: the DOMRect object for #id, or null if no such element exists.
 */
const getRect = (id) => document.getElementById(id) && document.getElementById(id).getBoundingClientRect();

/* "compass Rose" functions
 * ========================
 * Takes in an element #id, and returns the an object representing the {x, y} point for one of the eight respective "compass" rose directions (top, bottom, right, and left are N, S, E, W).
 * 
 * RETURNS: An object with two properties x and y as noted above and string describing which "point" on the rect this was. If no element with #id exists, each function returns null.
 * 
 * LOGIC FOR ALL 8 FUNCTIONS: we capture the value of getRect(id) to keep it DRY. Then we return the result of a boolean AND. If rect is null, the evaluation goes no further, and we return null. Else, we return the second value, the object.
 */
export const topEdge = (id) => {
	let rect = getRect(id);
	return rect &&
		{
			x: (rect.left + rect.right)/2 + window.scrollX,
			y: rect.top + window.scrollY,
			compassRoseDirection: "top"
		};
};

export const rightEdge = (id) => {
	let rect = getRect(id);
	return rect &&
		{
			x: rect.right + window.scrollX,
			y: (rect.top + rect.bottom)/2 + window.scrollY,
			compassRoseDirection: "right"
		};
};

export const bottomEdge = (id) => {
	let rect = getRect(id);
	return rect &&
		{
			x: (rect.left + rect.right)/2 + window.scrollX,
			y: rect.bottom + window.scrollY,
			compassRoseDirection: "bottom"
		};
};

export const leftEdge = (id) => {
	let rect = getRect(id);
	return rect &&
		{
			x: rect.left + window.scrollX,
			y: (rect.top + rect.bottom)/2 + window.scrollY,
			compassRoseDirection: "left"
		};
};

export const topLeftCorner = (id) => {
	let rect = getRect(id);
	return rect &&
		{
			x: rect.left + window.scrollX,
			y: rect.top + window.scrollY,
			compassRoseDirection: "top-left"
		};
};

export const topRightCorner = (id) => {
	let rect = getRect(id);
	return rect &&
		{
			x: rect.right + window.scrollX,
			y: rect.top + window.scrollY,
			compassRoseDirection: "top-right"
		};
};

export const bottomRightCorner = (id) => {
	let rect = getRect(id);
	return rect &&
		{
			x: rect.right + window.scrollX,
			y: rect.bottom + window.scrollY,
			compassRoseDirection: "bottom-right"
		};
};

export const bottomLeftCorner = (id) => {
	let rect = getRect(id);
	return rect &&
		{
			x: rect.left + window.scrollX,
			y: rect.bottom + window.scrollY,
			compassRoseDirection: "bottom-left"
		};
};

/* distanceBetween Utility function (not exported)
 * ===============================================
 * DRYs out the code for getting the distance between two {x, y} points.
 * 
 * INPUT ASSUMPTIONS: Points as {x,y} objects whose values are all Numbers.
 * 
 * RETURNS: the distance as a Number.
 */
const distanceBetween = (point1, point2) => Math.sqrt((point1.x-point2.x)**2 + (point1.y-point2.y)**2);


/* arrayOfPoints Utility Functions (not exported)
 * ==============================================
 * Takes in a space-delimited string of tokens defining which vertices and edges to include in the returned array.
 * The returned array will include a non-repeating UNION of the selections in the input strings. (e.g.: "sides top" includes all compass rose points Except the middle of the bottom edge).
 * All tokens comprised of multiple words will use hyphens instead of spaces.
 * Input is case-insensitive.
 * Tokens not listed below are ignored.
 * 
 * LIST OF TOKENS AND THEIR MEANINGS: 
 * "top" "bottom" "left" "right": returns the midpoint of the respective edge of the clientRect of the element.
 * "top-left" "top-right" "bottom-right" "bottom-left": returns the respective corner of the clientRect.
 * "sides": returns the six points on the left and right edges of the rectangle
 * "edges": equivalent to "top bottom right left"
 * "corners": equivalent to "top-left top-right bottom-right bottom-left"
 * "all": equivalent to "edges corners"
 */
const arrayOfPoints = (tokenString, id) => {
	//Let's start by eliminating case issues
	tokenString = tokenString.toLowerCase();
	
	//Expand any short-hand tokens in the lowercase version of the string.
	tokenString = tokenString.replace(/\ball\b/g,"edges corners");
	tokenString = tokenString.replace(/\bedges\b/g,"top right bottom left");
	tokenString = tokenString.replace(/\bcorners\b/g,"top-left top-right bottom-right bottom-left");
	tokenString = tokenString.replace(/\bsides\b/g,"top-left left bottom-left top-right right bottom-right");

	//Eliminate any duplicates or junk input by converting to an array, sorting, filtering by comparing to prior indices, and filtering again by our token list.
	const tokens = tokenString.split(" ").sort().filter(
			//arr[-1] is undefined, so index 0 will fail the second test.
			(token, idx, arr) => idx=0 || token !== arr[idx-1]
		).filter(
			token => (/^(top|right|bottom|left|top-left|top-right|bottom-right|bottom-left)$/.test(token))
		);
	
	//define an function lookup table for tokens:
	const pointFromToken = {
		top: topEdge(id),
		right: rightEdge(id),
		bottom: bottomEdge(id),
		left: leftEdge(id),
		'top-left': topLeftCorner(id),
		'top-right': topRightCorner(id),
		'bottom-right': bottomRightCorner(id),		
		'bottom-left': bottomLeftCorner(id)
	};

	//now let's map onto the points we need and return that array
	return tokens.map(token => pointFromToken[token]);	
};

/* closestPointsOfTwoRects export function
 * =======================================
 * Takes in ids for two HTML elements (id1, id2) that fit the box model and returns an object that represent the closest line between those two elements based on the presence and value of optional parameter pointString. 
 * 
 * DEFAULT BEHAVIOR: Iterate through the "compassRose" points for both elements, and find the shortest distance from every point on id1 to a point on id2. Next, find which point on id1 was closest to a point on id2, and return an object that represents that line.
 * 
 * EFFECT OF OPTIONAL PARAMETERS: pointString1 & pointString2 take a space-delimited token string that operates according to the documentation for the utility function arrayOfPoints() above. If both omitted, "all" is used for both. If only one is passed, it is used for both rectangles.
 * Possible values for PointString tokens:
 * all|edges|corners|sides|top|right|bottom|left|top-left|top-right|bottom-right|bottom-left
 * 
 * RETURNS: An object representation of the line - {start, end, distance}, or returns null if either id1 or id2 didn't correspond to an element on the page.
 */
export const closestPointsOfTwoRects = (id1, id2, pointString1, pointString2) => {
	// Basic input checking - return null if calculation of distance is impossible.
	if (!document.getElementById(id1) || !document.getElementById(id2)) {
		return null;
	}

	// Deal with presence or absence of optional parameter.
	if (pointString1 === undefined && pointString2 === undefined) {
		pointString1 = pointString2 = "all";
	} else if (pointString2 === undefined) {
		pointString2 = pointString1;
	}

	//Create the 2-d array containing an array of the desired compass rose points for each id.
	let compassRosePointsOfBothRects = [
		arrayOfPoints(pointString1, id1),
		arrayOfPoints(pointString2, id2)
	];

	//For every rect1 compass point in compassRosePointsOfBothRects[0] (the array of rose points for id1), we need to know which rect2point was closest, and how far it was.
	let perigeesOfCompassRosePointsOfBothRects = compassRosePointsOfBothRects[0].map(//for every id1 rect point
		(rect1Point) =>
			compassRosePointsOfBothRects[1].reduce(//pick the id2 rect point which is closest
				(closestRect2PointToRect1, rect2Point) =>
					closestRect2PointToRect1.distance <= distanceBetween(rect1Point, rect2Point)
						? closestRect2PointToRect1
						: {point: rect2Point, distance: distanceBetween(rect1Point, rect2Point)}
				, {point: {x:Infinity, y:Infinity}, distance: Infinity}
			)
	);
	
	//pick the id1 point which is the closest to id2.
	let lineBetweenClosestPoints = perigeesOfCompassRosePointsOfBothRects.reduce(
		(closestSoFar, perigeeToPointOnRect1, idx) =>
			closestSoFar.distance > perigeeToPointOnRect1.distance
				? {
					start: compassRosePointsOfBothRects[0][idx],
					end: perigeeToPointOnRect1.point,
					distance: perigeeToPointOnRect1.distance,
				  }
				: closestSoFar
		, {start: {}, end: {}, distance: Infinity}
	);
	
	return lineBetweenClosestPoints;
};

/* cornerAwareLine export function
 * =======================================
 * Wrapper function for closestPointsOfTwoRects that pushes the ends of the arrows in by @param "padding" px.
 */
export const cornerAwareLine = (padding, id1, id2, pointString1, pointString2) => {
	const line = closestPointsOfTwoRects(id1, id2, pointString1, pointString2);

	//conditional merely guards against the null case.
	 line && [line.start, line.end].forEach(point => {
		if (point.compassRoseDirection.includes("top")) point.y += padding;
		if (point.compassRoseDirection.includes("bottom")) point.y -= padding;
		if (point.compassRoseDirection.includes("left")) point.x += padding;
		if (point.compassRoseDirection.includes("right")) point.x -= padding;		
	});

	return line;
}