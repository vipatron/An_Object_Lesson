/**
 * Sets elem into arr[index1][index2] in a setState-friendly (immutable) way by returning updated nested copy of a 2-dimensional array. Out-of-bound indexes will result in a copy of the initial array.
 * @function setElement
 * @param  {[][]} arr a 2D array
 * @param  {number} index1 index of the subarray
 * @param  {number} index2 index at which to assign the element in the subarray
 * @param  {*} elem the element to assign
 */
export function set2DElement(arr, index1, index2, elem){
	return arr.map(
		(subArray, i) => i === index1
			? subArray.map(
				(oldElem, j) => j === index2
					? elem
					: oldElem
			)
			: subArray.slice()
	)
}

// Note: Because we have to copy the whole 2D array, these operations can be memory-costly, so use with caution for large arrays or non-primitive elements.
// Tip: If you are trying to update an element in a 1D array, homebrew a simpler one-off map() call instead.
// Possible Future Updates: If you need immutable 3d array functions, you'll have to write them, because these functions will mutate arrays greater than 3 dimensions.
