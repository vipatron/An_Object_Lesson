function memoizedGetScrollbarWidth(){
	let cachedWidth;
	return function(){
		if (!cachedWidth){
			const inner = document.createElement('p');
			inner.style.width = "100%";
			inner.style.height = "200px";
		  
			const outer = document.createElement('div');
			outer.style.position = "absolute";
			outer.style.top = "0px";
			outer.style.left = "0px";
			outer.style.visibility = "hidden";
			outer.style.width = "200px";
			outer.style.height = "150px";
			outer.style.overflow = "hidden";
			outer.appendChild (inner);

			document.body.appendChild(outer);
			const NoScrollbarParaWidth = inner.offsetWidth;
			outer.style.overflow= 'scroll';
			let ParaWidthWithScrollbar = inner.offsetWidth;
			if (NoScrollbarParaWidth === ParaWidthWithScrollbar) ParaWidthWithScrollbar = outer.clientWidth;
			document.body.removeChild(outer);
			cachedWidth = NoScrollbarParaWidth - ParaWidthWithScrollbar;
		}
		return cachedWidth;
	}
}

export default memoizedGetScrollbarWidth();