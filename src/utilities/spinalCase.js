/* spinalCase Utility Function
 * ===========================
 * Takes in a string, and returns the spinal case of the string: all lower case, no punctuation except underscore, and whitespace replaced with a hyphen.
 * Example: "Sid is a Floof"
 */
export const spinalCase = str =>
	str.split("")
		.filter(char => /[A-Za-z\s]/.test)
		.join("")
		.toLowerCase()
		.replace(/\s+/g, "-");

/** @description Similar to spinalCase function above, but retains the capitalization of the original string.
 *  @param {string} str The string to put into spinal case
 *  @return {string}
 */
export const retainCapsSpinalCase = str =>
	str.split("")
		.filter(char => /[A-Za-z\s]/.test)
		.join("")
		.replace(/\s+/g, "-");
/* retainCapsSpinalCase Utility Function
 * =====================================
 * @description Similar to spinalCase function above, but retains the capitalization of the original string.
 */

/* reverseSpinalCase Utility Function
 * ==================================
 * Takes in a string in spinal case, returns the string with hyphens replaced with single spaces.
 */
export const reverseSpinalCase = str =>
	str.replace(/-/g, " ");


export default spinalCase;