import React, { Component } from 'react';
import Arrow from './Arrows/Arrow';
import ArrowFromPoints from './Arrows/ArrowFromPoints';
import ArrowFromLine from './Arrows/ArrowFromLine';
// import Member from './Members/Member';
// import CenteredText from './Members/CenteredText';
// import LinkableMember from './Members/LinkableMember';
import ObjectRepresentation from './ObjectRepresentation/ObjectRepresentation';
// import makeDraggable from './utilities/makeDraggable';
import DraggableSVG from './DraggableSVG/DraggableSVG';

const arrowTestValues = {
	id: "arrow1",
	start: {x: 10, y: 10},
	end: {x:90, y: 90},
	color: "teal",
	strokeWidth: 5
}

const fromPointsTestValues = {
	id: "arrow2",
	line:{
		start: {x: 10, y: 110},
		end: {x:90, y: 190}
	},
	color: "orange",
	strokeWidth: 5
}

const fromLineTestValues = {
	id: "arrow3",
	line:{
		start: {x: 10, y: 210},
		end: {x:90, y: 290}
	},
	color: "magenta",
	strokeWidth: 5
}
function sleep(milliseconds) {
	const start = new Date().getTime();
	for (let i = 0; i < 1e7; i++) {
	  if ((new Date().getTime() - start) > milliseconds){
		break;
	  }
	}
  }


class SVGComponentTester extends Component{
	constructor(props){
		super(props);

		const svgNS = "http://www.w3.org/2000/svg";
		const el = document.createElementNS(svgNS, "svg");
		const newSVGText = document.createElementNS(svgNS, "text");
		newSVGText.innerHTML = "hey ho!";
		el.appendChild(newSVGText);
		document.body.insertBefore(el, document.getElementById("root"));
		newSVGText.setAttributeNS(null, "x", 100);
		newSVGText.setAttributeNS(null, "y", 100);
		newSVGText.setAttributeNS(null, "font-size", 50);
		el.remove();
	
		this.state = {
			width: 200,
			showObjects: true,
			dragged: { id: null },
		};
		this.toggleObjects = this.toggleObjects.bind(this);
		this.testMouseMovements = this.testMouseMovements.bind(this);
	}

	toggleObjects(){
		this.setState({showObjects: !this.state.showObjects});
	}

	/**
	 * the code here resizes the rect aroudn the text
	 */
	handleClick = evt => {
	// console.log(evt);
		const textNode = evt.target.parentNode.children[1];//since we define the Member component, we know that children[0]:rect, and [1]:text
		const width = textNode.getComputedTextLength();
		// console.log("constructor:", textNode.__proto__.constructor.toString());
		console.log(width, textNode.constructor.name, textNode);
		this.setState(state => ({width: width + 10}));//padding-left and -right are 5 this way. (TODO: make this equal to 30% of the font-size, like it is vertically, since that value should be the same for all these "divs" I've created.)
		// console.log(textNode, textNode.textLength, this.state.width, textNode.constructor)
	};

	componentDidMount(){
		// setTimeout(this.testMouseMovements, 0);
	}

	testMouseMovements(){
		// console.log("this:", this);
		// let x = 850, y = 70;
		const blockId = "O";
		const dx = -1;
		let dy = 600;
		const numAttempts = 200;
		// let x = 650, y = 450;
		// const blockId = "T";
		// const dx = 10;
		// let dy = 200;
		// const numAttempts = 24;

		const block = document.getElementById(blockId);
		console.log("Logged Output: SVGComponentTester -> testMouseMovements -> block", block)

		//this is a naive approach that doesn't account for svgTransforms to client-space, but it should be fine for this test if we use a 50-unit buffer.
		
		for (let attempt = 1; attempt <= numAttempts; attempt++){
			// setTimeout(() => console.clear() || console.log("attempt #", attempt));
			let clientRect = block.getBoundingClientRect();
			let {left, top} = clientRect;
			console.table({left, top});
			this.mockClickAndDrag({x: left+50, y: top+50}, {dx, dy}, blockId);
			clientRect = block.getBoundingClientRect();
			left = clientRect.left;
			top = clientRect.top;
			console.table({left, top});
			// if (attempt < numAttempts) this.mockClickAndDrag({x: left+50, y: top+50}, {dx: dx, dy: -100}, "T");
			this.mockClickAndDrag({x: left+50, y: top+50}, {dx: 0, dy: -20}, blockId);
			if (clientRect.bottom > 700){
				break;
			}
			dy += 10;
		}
		// this.mockClickAndDrag({x: 650, y: 450}, {dx: 60, dy: 0}, "T")
		// this.mockClickAndDrag({x: 710, y: 450}, {dx: 10, dy: 250}, "T")
		// setTimeout(() => console.log("===========DONE INITIAL MOUSE EVENTS==========="), 0);
		console.log("===========DONE INITIAL MOUSE EVENTS===========")
	}

	mockClickAndDrag(mouseDownCoordinates, mouseMoveDistances, targetGroupId){
		const {x: clientX, y: clientY} = mouseDownCoordinates;
		const {dx, dy} = mouseMoveDistances;
		const mouseDownEvent = new MouseEvent("mousedown", {
			clientX,
			clientY,
			bubbles: true,
		})
		const targetRect = document.getElementById(targetGroupId).firstChild;
		// setTimeout(() => console.log("%cmousedown dispatched?", "background: black; color: green", targetRect.dispatchEvent(mouseDownEvent)));
		console.log("%cmousedown dispatched?", "background: black; color: green", targetRect.dispatchEvent(mouseDownEvent))
		const mouseMoveEvent = new MouseEvent("mousemove", {
			clientX: clientX + dx,
			clientY: clientY + dy,
			bubbles: true,
		});
		// setTimeout(() => console.log("%cmousemove dispatched?", "background: black; color: green", targetRect.dispatchEvent(mouseMoveEvent)));
		console.log("%cmousemove dispatched?", "background: black; color: green", targetRect.dispatchEvent(mouseMoveEvent))
		const mouseUpEvent = new MouseEvent("mouseup", {
			bubbles: true,
		})
		// setTimeout(() => console.log("%cmouseup dispatched?", "background: black; color: green", targetRect.dispatchEvent(mouseUpEvent)), 0);
		console.log("%cmouseup dispatched?", "background: black; color: green", targetRect.dispatchEvent(mouseUpEvent))
	}

	render (){
		return <div>
		<button style={{position: "fixed"}} onClick={this.toggleObjects}>Show Objects</button>
		<DraggableSVG
			classesForDragging = {['blob']}
			ref={this.mainSVGref}
			allowOverlap={false}
			unitsOfOverlapNecessaryForTransparency={2}
			dragData={this.state.dragged}
			// unitsOfOverlapNecessaryForTransparency={1}
			svgChildDragHandler={()=>{}}
		>
			{/* <g id='P' className="blob">
				<rect x='300' y='300' width='200' height='200' fill='pink'/>
			</g> */}
			<g id='P' className="blob">
				<rect x='400' y='400' width='200' height='200' fill='pink'/>
			</g>
			<g id='O' className="blob">
				<rect x='800' y='20' width='200' height='200' fill='orange'/>
			</g>
			<g id='Q' className="blob">
				<rect x='700' y='700' width='200' height='200' fill='blue'/>
			</g>
			{/* <g id='R' className="blob">
				<rect x='20' y='100' width='200' height='200' fill='red'/>
			</g> */}
			{/* <g id='R' className="blob">
				<rect x='20' y='98' width='200' height='200' fill='red'/>
			</g> */}
			{/* <g id='R1' className="blob">
				<rect x='120' y='100' width='200' height='200' fill='red'/>
			</g>
			<g id='R2' className="blob">
				<rect x='480' y='100' width='200' height='200' fill='red'/>
			</g> */}
			{/* <g id='R1' className="blob">
				<rect x='300' y='100' width='200' height='200' fill='red'/>
			</g>
			<g id='R2' className="blob">
				<rect x='500' y='200' width='200' height='200' fill='red'/>
			</g>
			<g id='R0' className="blob">
				<rect x='80' y='100' width='200' height='200' fill='red'/>
			</g> */}
			<g id='R' className="blob">
				<rect x='200' y='200' width='200' height='200' fill='red'/>
			</g>
			<g id='S' className="blob">
				<rect x='100' y='600' width='200' height='200' fill='green'/>
			</g>
			<g id='T' className="blob">
				<rect x='700' y='450' width='200' height='200' fill='teal'/>
			</g>
			{/* <g id='slightlyBigger' className="blob">
				<rect x='500' y='299' width='200' height='201' fill='teal'/>
			</g> */}
			{/* <g id='Big' className="blob">
				<rect x='820' y='250' width='200' height='300' fill='teal'/>
			</g>
			<g id='Small' className="blob">
				<rect x='1040' y='350' width='200' height='100' fill='teal'/>
			</g> */}
			{/* <g id='AboveRight' className="blob">
				<rect x='600' y='180' width='200' height='200' fill='teal'/>
			</g> */}
			{/* <g id='Above' className="blob">
				<rect x='400' y='80' width='200' height='200' fill='teal'/>
			</g>
			<g id='Right' className="blob">
				<rect x='520' y='300' width='200' height='200' fill='teal'/>
			</g> */}
		</DraggableSVG>
		</div>
	}
}

export default SVGComponentTester;