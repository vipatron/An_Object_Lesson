import React from 'react';
import { PropTypes } from 'prop-types';
import {retainCapsSpinalCase} from '../utilities/spinalCase'

/**
 * Dropdown Functional Component
 * =============================
 * Renders a dropdown whose options are the array of strings provided as props.options. If no value is passed as props.initialValue, TODO: there is no preselected value? Let's experiment.
 */

const Dropdown = props =>{
console.log("DropDown rendered");
	return <select
		onChange={props.changeHandler}
		value={props.selectedIndex}
	>
		{props.children.map(
			(str, i) =>
				<option value={i} key={retainCapsSpinalCase(str)+i}>
					{str}
				</option>
		)}
</select>};

Dropdown.propTypes = {
	children: PropTypes.arrayOf(PropTypes.string).isRequired,
	selectedIndex: PropTypes.number,
	changeHandler: PropTypes.func,
};

export default Dropdown;