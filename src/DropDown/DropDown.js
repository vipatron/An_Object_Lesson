import React, { PureComponent } from 'react';
import { PropTypes } from 'prop-types';
import {retainCapsSpinalCase} from '../utilities/spinalCase'

/**
 * Dropdown class PureComponent
 * ============================
 * Renders a controlled dropdown menu.
 * 
 * IMPLEMENTATION:
 * props.options (string[]) are the displayed options, with "value" set to the string's index in the array.
 * props.selectedIndex is the displayed string's index, and the evt.target.value of this controlled <select> element.
 * 
 * LOGIC:
 * As a PureComponent of props, it saves on costly re-renders that require a map() call whose callback requires another function call.
 */

class Dropdown extends PureComponent{
	render(){
		return <select
			onChange={this.props.changeHandler}
			value={this.props.selectedIndex}
		>
			{this.props.children.map(
				(str, i) =>
					<option value={i} key={retainCapsSpinalCase(str)+i}>
						{str}
					</option>
			)}
		</select>;
	}
}

Dropdown.propTypes = {
	children: PropTypes.arrayOf(PropTypes.string).isRequired,
	selectedIndex: PropTypes.number,
	changeHandler: PropTypes.func,
};

export default Dropdown;