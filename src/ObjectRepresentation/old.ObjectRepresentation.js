import React from 'react';
import PropTypes from 'prop-types';

//import subcomponents
import Member from '../Members/Member.js';
import LinkableMember from '../Members/LinkableMember.js';

//import utility functions
import { retainCapsSpinalCase } from '../utilities/spinalCase.js';


/* ObjectRepresentation Functional Component
 * =========================================
 * Represents a Javascript object, with props.children being the name of the Object, composing the "own" properties as Member Components, and the properties involved in the OOJS hierarchy as LinkableMember Components.
 *
 * IMPLEMENTATION:
 * It has nested Members for 2, possibly 3 "member properties" as applicable:
 * prototype: the object which will be used as a template if this object is a constructor
 * constructor: the function, which when used as a constructor, created this object.
 * __proto__: the object from which this one was templated by the constructor.
 *
 * APP-SPECIFIC LOGIC:
 * If props.isConstructor is true, display the member div for "prototype", which only constructors have.
 * TODO: if props.displayFunctionConstructor is true, display the member divs for "_proto" and "constructor" on the Constructor representations as well.
 */
const ObjectRepresentation = props => {
	let idRoot = retainCapsSpinalCase(props.children);
	let members = ["__proto__"];
	if (props.isConstructor){
		if (!props.displayFunctionConstructor){
			members=[];
		}	
		members.unshift("prototype");
	} else if (props.isPrototypeInTree){
		members = members.concat("constructor");
	}
	let inheritanceMembersJSX = members.map(member => {
		// store the id to avoid two calls to spinalcase and for semantics
		const id = idRoot+"-"+member;
/* 		console.log("id:",id); */
		// if the id is the protoChain root, whose value is null, LinkableMember needs to know in order to style it correctly.
		return <LinkableMember
			key={id}
			isProtoChainRoot={(id === "prototype-for-new-Objects-__proto__")}
			parentObjectName={idRoot}
			dragStartHandler={props.dragStartHandler}
			dragEndHandler={props.dragEndHandler}
		>
			{member}
		</LinkableMember>
	});
	let genericMembersJSX = props.members
		? props.members.map((member, idx) =>
				<Member key={`${member}-${idx}`}>{member}</Member>
			)
		: [];

	return (
		<div
			style={{marginLeft: props.offset}}
			id={idRoot}
			className="ObjectRepresentation"
			onDragOver={props.dragOverHandler}
			onDrop={props.dropHandler(idRoot)}
		>
			{props.children}
			{inheritanceMembersJSX}
			{genericMembersJSX}
		</div>
	);
};

ObjectRepresentation.propTypes = {
	children: PropTypes.string.isRequired,
	isConstructor: PropTypes.bool,
	isPrototypeInTree: PropTypes.bool,
	displayFunctionConstructor: PropTypes.bool,
	members: PropTypes.arrayOf(PropTypes.string),
	dragStartHandler: PropTypes.func,
	dragEndHandler: PropTypes.func,
	dragOverHandler: PropTypes.func.isRequired,
	dropHandler: PropTypes.func.isRequired

};

export default ObjectRepresentation;