import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import './ObjectRepresentation.css';

//import subcomponents
import Member from '../Members/Member.js';
import LinkableMember from '../Members/LinkableMember.js';

//import utility functions
import { retainCapsSpinalCase } from '../utilities/spinalCase.js';
import CenteredText from '../Members/CenteredText.js';

/* ObjectRepresentation Stateful Class PureComponent
 * =================================================
 * Represents a Javascript object, with props.children being the name of the Object, composing the "own" properties as Member Components, and the properties involved in the OOJS hierarchy as LinkableMember Components.
 *
 * 
 * IMPLEMENTATION:
 * It sets its own state.width based on the computed widths of the SVG text representing its members (in the constructor, since they shouldn't change without changing props).
 * It has nested Members for 2 or 3 "inheritance member properties" as applicable:
 * prototype: the object which will be used as a template if this object is a function used as a constructor.
 * constructor: the function, which when used as a constructor, created this object.
 * __proto__: the object from which this one was templated by the constructor.
 *
 * APP-SPECIFIC LOGIC:
 * If props.isConstructor is true, display the member div for "prototype", which only constructors have.
 * 
 * 
 * TODO: organize methods.
 */

class ObjectRepresentation extends PureComponent {
	constructor(props){
		super(props);
		const {prototypeNamePrefix, prefixFontSize, displayedName} = ObjectRepresentation.getObjectNameAndPrefixInfo(props);
		//all of the state is derived from props, but in the spirit of limiting re-renders and avoiding unnecessary calculation during those re-renders, all the costly processing is stored in this object's state.
		this.state = {
			prototypeNameLine: prototypeNamePrefix,
			prototypeLineFontSize: prefixFontSize,
			name: displayedName,
			objectWidth: ObjectRepresentation.getWidth(props),
			inheritanceMembers: ObjectRepresentation.getInheritanceMembers(props)
		};
	}

	/**
	 * Gets the predicted height of an object panel
	 * @function ObjectRepresentation.getHeight
	 * @param {Object} [props] -  a props object, or an object with members that would be attributes of an ObjectRepresentation. If not passed, returns 90, the height of a basic instance.
	 */
	static getHeight({members, isPrototypeInTree, isConstructor, memberHeight, interMemberSpacing} = this.defaultProps){
		let numMembers = members ? members.length : 0;
		if (isPrototypeInTree) numMembers += 2;//.constructor, .__proto__
		if (isConstructor) numMembers += 1;//.prototype
		else if (!isPrototypeInTree) numMembers = 1;//leafs have .__proto__
		memberHeight = memberHeight || this.defaultProps.memberHeight;
		interMemberSpacing = interMemberSpacing || this.defaultProps.interMemberSpacing;

		//1 more interval for name + fencepost problem + half-height interval if a pure Protoype (not a constructor)
		return (1 + numMembers) * (memberHeight+interMemberSpacing) + interMemberSpacing + (isPrototypeInTree && !isConstructor ? memberHeight/2 + interMemberSpacing : 0);
	}


	/**
	 * Gets the predicted width of an object panel
	 * @function ObjectRepresentation.getWidth
	 * @param {Object} [props] - a props object, or an object with members that would be attributes of an ObjectRepresentation. If not passed, returns 140, the minimum width of a basic instance.
	 * @returns {number} - width of the putative ObjectRepresentation in SVG units.
	 */
	static getWidth(props={}){
		/* IMPLEMENTATION NOTES:
		 * When called from the constructor, React has already merged defaultProps with props where necessary: any properties in props override those in defaultProps. Any properties in neither are undefined.
		 * If for some reason no parameters are passed (when calling from App.js or elsewhere), defaultProps keeps the function from throwing errors, but since displayedName—a member returned from getObjectaNameAndPrefixInfo()—will be the string "undefined", that will evalute to ~140 SVG units wide, which is based on the width of a basic Instance's .__proto__ member.
		 * example props needed for full description of an OR with default styling:
			{
				isPrototypeInTree,
				isConstructor,
				displayFunctionConstructor: "not required for getHeight",
				members: "can be some falsy value to indicate no OwnProperties",
				children
			}
		 */
// console.log("in getWidth(), props:", JSON.stringify(props));

//PICK UP HERE: defaultProps isn't hydrating props without React, so we will.
for (let prop in this.defaultProps){
	if (props[prop] === undefined){
		props[prop] = this.defaultProps[prop];
	}
}
// Object.assign(props, this.defaultProps);

// console.log("in getWidth(), props:", Object.assign({}, props));
		//Dynamically create an SVG with a text node, add it invisibly to the document, and determine the max length from that. This works because the new svg has a scale of 1:1. We could verify that by dividing by SVGElem.currentSclae once it is appended to the document.body, but there is no need as it is the default.
		const SVGElem = document.createElementNS("http://www.w3.org/2000/svg", "svg");
		const SVGText = document.createElementNS("http://www.w3.org/2000/svg", "text");
		SVGElem.appendChild(SVGText);
		document.body.appendChild(SVGElem);

		//create a single array of all members, regardless of type
		const inheritanceMembers = this.getInheritanceMembers(props);
		const allMembers = inheritanceMembers.concat( props.members ? props.members : [] );

		//iterate over allMembers[], and figure out the maxWidth
		let maxTextWidth = 0;
		SVGText.setAttributeNS(null, "font-size", props.memberFontSizeInPixels);
		for (let i=0; i<allMembers.length; i++){
			SVGText.innerHTML = allMembers[i];
			maxTextWidth = Math.max(maxTextWidth, SVGText.getBoundingClientRect().width);
		}
		//the members have padding on the sides of their text
		const maxMemberWidth = maxTextWidth + 2*props.padding;
// console.log("maxMemberWidth", maxMemberWidth)

		//account for name & prefix lines' width, too
		const {prototypeNamePrefix, prefixFontSize, displayedName} = ObjectRepresentation.getObjectNameAndPrefixInfo(props);
		SVGText.innerHTML = displayedName;
		SVGText.setAttributeNS(null, "font-size", props.nameFontSizeInPixels);
		maxTextWidth = Math.max(maxTextWidth, SVGText.getBoundingClientRect().width);
		SVGText.innerHTML = prototypeNamePrefix;
		SVGText.setAttributeNS(null, "font-size", prefixFontSize);
		maxTextWidth = Math.max(maxTextWidth, SVGText.getBoundingClientRect().width);

		//if the members (including padding) are wider than the name & prefix, use them as the basis for object width.
		if (maxTextWidth < maxMemberWidth) maxTextWidth = maxMemberWidth;

		//clean up
		document.body.removeChild(document.body.lastChild);
		return maxTextWidth + 2 * props.padding;
	}

	/**
	 * Gets a string[] with the names of the inheritance members based on props.
	 * @param {Object} [props] - a props object, or an object with members that would be the 3 boolean attributes of an ObjectRepresentation. If not passed, returns ["__proto__"], the 
	 * @returns {string[]} - names of the inheritance members 
	 */
	static getInheritanceMembers({isPrototypeInTree, isConstructor, displayFunctionConstructor} = this.defaultProps){
		const inheritanceMembers = ["__proto__"];
		if ( isPrototypeInTree){
			inheritanceMembers.push("constructor");
		} else if (isConstructor){
			if (displayFunctionConstructor){
				inheritanceMembers.push("constructor");
			} else {
				inheritanceMembers.shift();//gets rid of "__proto__"
			}
			inheritanceMembers.push("prototype");
		}
		return inheritanceMembers;
	}

	/**
	 * @summary Returns some pieces of state derived from props needed to get estimated width and to render.
	 * @description Returns: the small prefix line to be displayed above the pseudoclass name, the size of the font to use for that line, and the main ObjectRepresentation name, including parentheses or trailing "s".
	 * @param  {Object} [props]
	 * @returns {Object} - { prototypeNamePrefix, prefixFontSize, displayedName }
	 */
	static getObjectNameAndPrefixInfo(props = this.defaultProps){
		return {
			prototypeNamePrefix: props.isPrototypeInTree ? "prototype object for new" : "",
			prefixFontSize: props.prototypePrefixFontSizeInPixels || props.nameFontSizeInPixels / 2,
			displayedName: props.children + (props.isPrototypeInTree
				? "s"
				: props.isConstructor
					? "()"
					: "")
		};
	}

	render(){
		const MEMBER_WIDTH = this.state.objectWidth - 2*this.props.padding;
		let numLinesOfText = this.props.isPrototypeInTree ? 0.5: 0;//name prefix for prototypes
		const nextVerticalOffset = () => (this.props.memberHeight + this.props.interMemberSpacing) * numLinesOfText++ + this.props.interMemberSpacing;
		const nameVerticalOffset = nextVerticalOffset();
	
		let idRoot = retainCapsSpinalCase(this.props.children);
		if (this.props.isPrototypeInTree) idRoot += "-prototype";
		else if (this.props.isConstructor) idRoot += "-constructor";

		const inheritanceMembersJSX = this.state.inheritanceMembers.map(member => {
			// store the id to avoid two calls to spinalcase and for semantics
			const id = idRoot+"-"+member;
			let rectColoring;
			if (id === this.props.dragData){
				rectColoring = "object__member-rect--dragging";
			} else if (this.props.arrowData.has(id)) {//else if id is in the list of already-linked members => green. This works because all ObjectReps re-render at the beginning or end of a drag thanks to dragData switching from null to the id of the dragged LinkableMember
				rectColoring = "object__member-rect--linked";
			}
			// if the id is the protoChain root, whose value is null, LinkableMember needs to know in order to style it correctly.
			const isProtoChainRoot = this.props.isPrototypeInTree &&
				this.props.children === "Object" &&
				member === "__proto__";
			return (
				<LinkableMember
					key={id}
					isProtoChainRoot={ isProtoChainRoot }
					parentObjectName={idRoot}
					dragStartHandler={this.props.memberDragStartHandler}
					x={this.props.padding} y={nextVerticalOffset()}
					width={MEMBER_WIDTH} height = {this.props.memberHeight}
					fontSize={this.props.memberFontSizeInPixels}
					rectClass={rectColoring}
			>
					{member}
				</LinkableMember>
			)
		});
		const genericMembersJSX = this.props.members
			? this.props.members.map((member, idx) =>
					<Member
						key={`${member}-${idx}`}
						x={this.props.padding} y={nextVerticalOffset()}
						width={MEMBER_WIDTH} height = {this.props.memberHeight}
						fontSize={this.props.memberFontSizeInPixels}
						>
						{member}
					</Member>
				)
			: [];
	
		
		return (
			<g
				className={"object" + (this.props.dragData ? " object__cursor--droppable" : "")}
				id={idRoot}
				transform={`translate(${this.props.x}, ${this.props.y})`}
			>
				<rect
					className="object__panel"
					width={this.state.objectWidth}
					height={nextVerticalOffset()}
				/>
				{this.props.isPrototypeInTree &&
					<CenteredText
						classes="object__name-proto-prefix"
						width={this.state.objectWidth} height={this.props.memberHeight/2}
						fontSize={this.state.prototypeLineFontSize}
						y={this.props.padding}
					>
						{this.state.prototypeNameLine}
					</CenteredText>
				}
				<CenteredText
					classes="object__name"
					width={this.state.objectWidth} height={this.props.memberHeight}
					fontSize={this.props.nameFontSizeInPixels}
					y={nameVerticalOffset}
				>
					{this.state.name}
				</CenteredText>
				{inheritanceMembersJSX}
				{genericMembersJSX}
			</g>
		);
	}
}
/*PICK UP HERE: go through, add isRequired, take off only if errors*/
ObjectRepresentation.propTypes = {
	//content
	children: PropTypes.string.isRequired,
	members: PropTypes.arrayOf(PropTypes.string),
	isConstructor: PropTypes.bool,
	isPrototypeInTree: PropTypes.bool,
	displayFunctionConstructor: PropTypes.bool,
	//drag handlers/data pass-down
	arrowData: PropTypes.object.isRequired,
	dragData: PropTypes.string,
	memberDragStartHandler: PropTypes.func.isRequired,

	//presentation
	x: PropTypes.number.isRequired,
	y: PropTypes.number.isRequired,
	padding: PropTypes.number,
	memberHeight: PropTypes.number,
	interMemberSpacing: PropTypes.number,
	nameFontSizeInPixels: PropTypes.number,
	prototypePrefixFontSizeInPixels: PropTypes.number,
	memberFontSizeInPixels: PropTypes.number,
};

ObjectRepresentation.defaultProps = {
	padding: 18,
	memberHeight: 30,
	interMemberSpacing: 10,
	nameFontSizeInPixels: 20,
	memberFontSizeInPixels: 16,
};


export default ObjectRepresentation;