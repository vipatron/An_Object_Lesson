import React, { Component } from 'react';
import './App.css';

//import Components
import ArrowFromLine from './Arrows/ArrowFromLine.js';
import ObjectRepresentation from './ObjectRepresentation/ObjectRepresentation.js'

//import utility fxns.
import { closestPointsOfTwoRects } from './utilities/rect-boundaries';
import { spinalCase, retainCapsSpinalCase, reverseSpinalCase } from './utilities/spinalCase.js'

console.clear();
/*TODO:
 + implement draggability onto ORs for just the prototype
 + implement draggability for all members (copy/paste div attributes)
 + implement some horizontal spacing based on place in the array.
 + Try to DRY out member code.

 + implement selective draggability depending on the semantics of the connection (e.g: can only drop a constructor arrow onto Constructor Object).
 + Implement a complexity switch. (to toggle off "constructor" and "prototype" members in Constructors until we introduce Function() as the constructor for Constructors)
 - Implement some draggability of Objects to straighten out arrow by procedure or by user.
 
 - Implement some way of figuring out which orientation is the one normal to the target
 ~ Implement another kind of Arrow (path as arc?) for reflexive links.
 + Improve UI by making the ends of the arrows overlap their endpoints.

 */

/* App Container Component
 * =======================
 * Holds the state for the objects in the prototype chain, the arrows created by the user between them, and the window width to trigger re-renderings of those arrows.
 */
class App extends Component {
	constructor(props){
		super(props);
		//windowWidth is only meant to trigger a re-render without using forceUpdate, and to potentially use that information in the future.
		this.state = {
			FunctionConstructorsDisplayed: true,
			windowWidth: 0,
			arrows: new Map(),
			lastDropSuccessful: false,
			//if isConstructor is true, .constructor is Function().
			//__proto__ is .constructor.prototype (triangular relationships)
			//all objects listed here are either:
				//1: ConstructorFunctions()
				//2: nonPrototypeInstances (i.e.:leafs of prototype tree)
			objects: {
				Object: {
					isConstructor: true,
					prototype:{
						constructor: null
					}
				},
				Animal: {
					isConstructor: true,
					prototype:{
						members:["eat","replicate"],
						constructor: "Object"
					}
				},
				Carnivore: {
					isConstructor: true,
					prototype:{
						members:["kill", "hunt"],
						constructor: "Animal"
					}
				},
				Cat: {
					isConstructor: true,
					prototype:{
						members: ["meow"],
						constructor: "Carnivore"
					}
				},
				"sid The Butt": {
					isConstructor: false,
					constructor: "Cat"
				}
			}
		};	
	}
	
	componentDidMount(){
		window.addEventListener("resize", this.updateWidth);
	}
	
	componentWillUnmount() {
		window.removeEventListener("resize", this.updateWidth);
	}
	
	updateWidth = () => {
		this.setState({windowWidth: window.innerWidth});
	};
	
	/* dragStartHandler
	 * ================
	 * De
	 */
	dragStartHandler = evt => {
		evt.dataTransfer.setData("droppedId", evt.target.id);
		evt.dataTransfer.effectAllowed = 'link';
		evt.target.className = evt.target.className.replace(/ linked/g, "") + " dragging";
	};

	/* Can't move this logic to drop, because if the "drop" doesn't occur on a drag target, then dragEnd handler will fire, but not drop.
	 * This will fire in all 3 cases:
	 * 		Dropped onto a DragTarget:
	 * 			Successful linkage by user.
	 *  		Unsuccessful linkage by user.
	 * 		Dropped onto the backround (not a drag target),
	 * so it's the correct place to reset the lastDropSuccessful flag (to false) XOR to clean up any arrows that previously started at the element that was dragged (evt.target.id).
	 */
	dragEndHandler = evt => {
		console.log("IN DRAG END");
		console.log("dropEffect:", evt.dataTransfer.dropEffect);
		let replacementClass;
		if (this.state.lastDropSuccessful){
			console.log("DROP SUCCESSFUL");
			replacementClass = " linked";
			this.setState({ lastDropSuccessful: false });
		} else {
			console.log("DROP FAILED");
			//Drop failed: try deleting the arrow which starts at the dragged element. If such an arrow exists, the delete call will return true, and we should now update state with the altered Map. 
			this.state.arrows.delete(evt.target.id) && this.setState({
					arrows: this.state.arrows
			});
			replacementClass = "";
		}
		evt.target.className = evt.target.className.replace(/ dragging| linked/g, replacementClass);
	}
	
	allowDrop = evt => {
		evt.preventDefault();
	};	

	drop = targetElem => evt => {
		console.log("IN DROP");
		let droppedId = evt.dataTransfer.getData("droppedId");
		let dropType = droppedId.slice(droppedId.lastIndexOf("-")+1);
		console.log("droppedId:", droppedId, "dropType:", dropType);

		let correctTarget;
		switch(dropType){
			case("prototype"):
				correctTarget = retainCapsSpinalCase("prototype for new "+droppedId.replace(/-prototype$/, "s"));
				console.log ("for prototype, correctTarget:", correctTarget);
				break;
			case("constructor"):
				correctTarget = /^prototype-for-new-(\w+)s-constructor$/.exec(droppedId)[1];
/* 				console.log ("for constructor, correctTarget:", correctTarget); */
				break;
			case("__proto__"):
				let objectWhoseProtoItIs = reverseSpinalCase(droppedId.replace(/-__proto__$/, ""));
				console.log("objectWhoseProtoItIs:", objectWhoseProtoItIs);
				//if the object whose __proto__ property it is is a prototype...
				if (/^prototype for new /.test(objectWhoseProtoItIs)){
					//we can strip out the prototypal object whose __proto__ is being dragged, then look in this.state.objects to get which Constructor was used to create this prototype object
					let constructorOfObjectWhoseProtoItIs = this.state.objects[
						/^prototype for new (\w+)s$/.exec(objectWhoseProtoItIs)[1]
					].prototype.constructor;
					correctTarget = retainCapsSpinalCase(
						`prototype for new ${constructorOfObjectWhoseProtoItIs}s`
					);
				} else {//the object whose __proto__ property it is was an non-prototype-chain instance
					correctTarget = retainCapsSpinalCase("prototype for new "+
						this.state.objects[objectWhoseProtoItIs].constructor+"s");
				}
/* 				console.log ("for __proto__, correctTarget:", correctTarget); */
				break;
			default:
				break;
		}

		console.log(`arrow from ${droppedId} to ${targetElem}`);
		console.log("+++++++++++++++++");

		let userLinkedCorrectly = correctTarget === targetElem;
		if (userLinkedCorrectly){
			console.log("Drop hit the correct Target");
			this.setState({
				arrows: this.state.arrows.set(droppedId, {
					target: targetElem,
					inProtoTree: dropType === "__proto__"
				}),
				lastDropSuccessful: userLinkedCorrectly
			});
		} else {
			this.setState({
				lastDropSuccessful: userLinkedCorrectly
			});
		}

/*		Maybe play a beep/flash a color if incorrect? */
	};
	
	render() {
		let arrowsJSX = [];
		//The line below works because the set of arrows maps many-to-one.
		this.state.arrows.forEach((value, key) => arrowsJSX.push(
			<ArrowFromLine
				key={key}
				id={`${key}-to-${value.target}`}
				line={closestPointsOfTwoRects(key, value.target, "left right", "edges")}
				color={value.inProtoTree ? 'orange' : 'darkturquoise'}
				strokeWidth={4}
			/>
		));
		let { objects } = this.state;
		const constructorsAndInstancesJSX = [];
		const prototypesJSX = [];
		Object.keys(this.state.objects).forEach((objKey, idx) => {
			constructorsAndInstancesJSX.push(
				<ObjectRepresentation
					key={spinalCase(objKey)+idx}
					isConstructor={objects[objKey].isConstructor}
					displayFunctionConstructor={this.state.FunctionConstructorsDisplayed}
					dragStartHandler={this.dragStartHandler}
					dragEndHandler={this.dragEndHandler}
					dropHandler={this.drop}
					dragOverHandler={this.allowDrop}
					offset={(idx+1) * 10}
				>
					{objKey}
				</ObjectRepresentation>
			);
			if (objects[objKey].isConstructor){
				prototypesJSX.push(
					<ObjectRepresentation
						key={spinalCase(objKey)+"proto"+idx}
						members={objects[objKey].prototype.members}
						isPrototypeInTree
						dragStartHandler={this.dragStartHandler}
						dragEndHandler={this.dragEndHandler}
							dropHandler={this.drop}
						dragOverHandler={this.allowDrop}
						offset={0}
					>
						{`prototype for new ${objKey}s`}
					</ObjectRepresentation>
				);
			}
		});


		/*PICK UP HERE?*/
		// let FunctionConstructor = {
		// 	Function: {
		// 		isConstructor: true,
		// 		prototype:{
		// 		constructor: Object.
		// 	}
		// }
		return (
		  <div className="App">
		  	<div id="left-column" className="column">
				{constructorsAndInstancesJSX}
			</div>
			<div className="spacer-column"/>
			<div id="right-column" className="column">
				{prototypesJSX}
			</div>
			{arrowsJSX}
		  </div>
		);
	}
}

export default App;
